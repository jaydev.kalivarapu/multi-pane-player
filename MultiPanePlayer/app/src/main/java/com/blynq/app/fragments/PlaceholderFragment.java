package com.blynq.app.fragments;

import android.annotation.TargetApi;
import android.app.Fragment;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.blynq.app.activities.R;

/**
 * Created by Jaydev on 18/07/16.
 */

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class PlaceholderFragment extends Fragment {

    private static final String HEIGHT = "Height";
    private static final String WIDTH = "Width";
    private static final String LEFT_MARGIN = "left";
    private static final String RIGHT_MARGIN = "right";
    private static final String TOP_MARGIN = "top";
    private static final String BOTTOM_MARGIN = "bottom";

    // Test Data
    private static final String INDEX = "FragNumber";


    /**
     * @param widthInDP
     * @param heightInDP
     * @param leftMarginDp
     * @param topMarginDp
     * @param rightMarginDp
     * @param bottomMarginDp
     * @param fragIndex
     * @return
     */
    public static PlaceholderFragment newInstance(int widthInDP,
                                                  int heightInDP, int leftMarginDp, int topMarginDp,
                                                  int rightMarginDp, int bottomMarginDp, int fragIndex) {

        PlaceholderFragment resizableFragment = new PlaceholderFragment();

        Bundle args = new Bundle();

        // Test purpose Only
        args.putInt(INDEX, fragIndex);

        // Set Width Height dynamically
        args.putInt(WIDTH, widthInDP);
        args.putInt(HEIGHT, heightInDP);

        // SetMargin
        args.putInt(LEFT_MARGIN, leftMarginDp);
        args.putInt(RIGHT_MARGIN, rightMarginDp);
        args.putInt(TOP_MARGIN, topMarginDp);
        args.putInt(BOTTOM_MARGIN, bottomMarginDp);
        resizableFragment.setArguments(args);

        return resizableFragment;
    }

    public PlaceholderFragment() {
    }

    public int dpToPx(int dp) {
        DisplayMetrics displayMetrics = getActivity().getResources().getDisplayMetrics();
        int px = Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
        return px;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_main, container,
                false);

        FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) rootView
                .getLayoutParams();

        // set width height
        params.height = dpToPx(getArguments().getInt(HEIGHT));
        params.width = dpToPx(getArguments().getInt(WIDTH));

        // substitute
        // parameters for left,top, right, bottom
        params.setMargins(dpToPx(getArguments().getInt(LEFT_MARGIN)),
                dpToPx(getArguments().getInt(TOP_MARGIN)),
                dpToPx(getArguments().getInt(RIGHT_MARGIN)),
                dpToPx(getArguments().getInt(BOTTOM_MARGIN)));
        rootView.setLayoutParams(params);

        // Test purpose Only
        switch (getArguments().getInt(INDEX)) {
            case 1:
                rootView.setBackgroundColor(Color.MAGENTA);

                ((TextView) rootView.findViewById(R.id.frag_index))
                        .setText("Fragment # " + getArguments().getInt(INDEX));

                break;

            case 2:
                rootView.setBackgroundColor(Color.GREEN);

                ((TextView) rootView.findViewById(R.id.frag_index))
                        .setText("Fragment # " + getArguments().getInt(INDEX));

                break;

            case 3:
                rootView.setBackgroundColor(Color.BLUE);

                ((TextView) rootView.findViewById(R.id.frag_index))
                        .setText("Fragment # " + getArguments().getInt(INDEX));

                break;

            default:
                rootView.setBackgroundColor(Color.WHITE);

                ((TextView) rootView.findViewById(R.id.frag_index))
                        .setText("Fragment # " + getArguments().getInt(INDEX));

                break;
        }

        return rootView;
    }
}

