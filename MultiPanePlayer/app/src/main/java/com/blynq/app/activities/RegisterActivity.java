package com.blynq.app.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.TextView;

import com.blynq.app.properties.PlayerProperties;
import com.blynq.app.server.ServerComm;
import com.blynq.app.tools.ALogger;
import com.blynq.app.tools.StorageTools;
import com.blynq.app.tools.UpdateApi;

import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;


public class RegisterActivity extends Activity {

    private TextView staticTextView;
    private TextView androidIdView;
    private SharedPreferences sharedpreferences;
    public static final String MyPREFERENCES = "MyPrefs";
    private static Logger LOGGER;
    private ScheduledExecutorService worker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        hideSystemUI();
        StorageTools.verifyPlayerDirectoryStructure();
        StorageTools.clearTempFolder();
        LOGGER = ALogger.getLogger(RegisterActivity.class);
        initializeStaticProperties();

        setContentView(R.layout.activity_register);
        staticTextView = (TextView) findViewById(R.id.textView);
        androidIdView = (TextView) findViewById(R.id.textView2);

        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        worker = Executors.newSingleThreadScheduledExecutor();

        Runnable updater = new Runnable() {
            @Override
            public void run() {
                UpdateApi.checkUpdate();
            }
        };

        if (sharedpreferences.contains("isPlayerRegistered") && sharedpreferences.getBoolean("isPlayerRegistered", false)) {
            LOGGER.info("Device already registered, launching player !");
            Intent pushIntent = new Intent(getApplicationContext(), MainPlayer.class);
            startActivity(pushIntent);
            finish();
        } else {
            RetrieveFeedTask retrieveFeedTask = new RetrieveFeedTask(getBaseContext());

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
                retrieveFeedTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            else
                retrieveFeedTask.execute();
        }

//        worker.schedule(updater, 1, TimeUnit.SECONDS);
    }

    private void initializeStaticProperties() {
        PlayerProperties.androidId = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);

        DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getRealMetrics(displaymetrics);
        PlayerProperties.screenHeight = displaymetrics.heightPixels;
        PlayerProperties.screenWidth = displaymetrics.widthPixels;
    }


    class RetrieveFeedTask extends AsyncTask<String, String, Void> {

        private Context context;

        public RetrieveFeedTask(Context context) {
            this.context = context;
        }

        @Override
        protected Void doInBackground(String... strings) {

            Map<String, String> jsonEntries = new HashMap<>();
            jsonEntries.put("device_key", PlayerProperties.androidId);
            final String jsonRequestString = new JSONObject(jsonEntries).toString();
            boolean registered = false;

            try {
                LOGGER.info("Checking if player is registered, requesting server !");
                final String urlString = PlayerProperties.baseUrl + PlayerProperties.registerUrlAppender;

                LOGGER.info("Sending request with URL " + urlString + " and json - " + jsonRequestString);
                JSONObject jsonObject = ServerComm.postToServer(urlString, jsonRequestString);
                registered = (Boolean) jsonObject.get("success");

            } catch (IOException e) {
                LOGGER.error("Could not connect to server, check internet connectivity ", e);
            } catch (JSONException e) {
                LOGGER.error("Exception while parsing registration response ", e);
            }

            if (registered) {
                sharedpreferences.edit().putBoolean("isPlayerRegistered", true).commit();
                LOGGER.info("Device is registered, launching Player ! ");
                Intent pushIntent = new Intent(context, MainPlayer.class);
                startActivity(pushIntent);
                finish();
            } else {
                publishProgress();
            }

            return null;
        }

        @Override
        protected void onProgressUpdate(String... values) {
            staticTextView.setText("Please register this screen with activation key as ");
            androidIdView.setTypeface(null, Typeface.BOLD);
            androidIdView.setText(PlayerProperties.androidId);
        }
    }

    private void hideSystemUI() {
        View decorView = getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
                | View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
                | View.SYSTEM_UI_FLAG_IMMERSIVE;
        decorView.setSystemUiVisibility(uiOptions);
    }

    public int getStatusBarHeight() {
        int statusBarHeight = 0;

        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            statusBarHeight = getResources().getDimensionPixelSize(resourceId);
        }

        return statusBarHeight;
    }


}
