package com.blynq.app.tools;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

/**
 * Created by Jaydev on 09/06/16.
 */
public class MediaOptimize {

    public static Bitmap reduceResolution(String filePath, int viewWidth, int viewHeight) {
        int reqHeight = viewHeight;
        int reqWidth = viewWidth;

        BitmapFactory.Options options = new BitmapFactory.Options();

        // First decode with inJustDecodeBounds=true to check dimensions
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(filePath, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        System.gc();                                        // TODO - remove explicit gc calls
        return BitmapFactory.decodeFile(filePath, options); // TODO - ignoring aspect ratio here, to use that for better optimisation
    }

    private static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {

        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            // Calculate ratios of height and width to requested height and width
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);

            // Choose the smallest ratio as inSampleSize value, this will guarantee
            // a final image with both dimensions larger than or equal to the
            // requested height and width.
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }
        return inSampleSize * 2;  // TODO Better optimisation necessary
    }
}
