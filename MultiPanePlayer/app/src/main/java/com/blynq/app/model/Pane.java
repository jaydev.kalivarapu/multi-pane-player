package com.blynq.app.model;

import com.itextpdf.awt.geom.Rectangle;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.io.Serializable;

/**
 * Created by Jaydev on 10/07/16.
 */
public class Pane implements Serializable {

    // Rectangle dimensions are in percentage of screen dimension
    private Rectangle rectangle;

    private int layout_pane_id;

    private PlayerUtils.OrientationType orientation;

    public Pane() {
    }

    public Pane(Rectangle rectangle, int layout_pane_id, String orientation) {
        this.rectangle = rectangle;
        this.layout_pane_id = layout_pane_id;
        this.orientation = PlayerUtils.getOrientationType(orientation);
    }

    @Override
    public boolean equals(Object other) {
        if (other == null) return false;
        if (other == this) return true;
        if (!(other instanceof Pane)) return false;
        Pane otherPane = (Pane) other;

        return new EqualsBuilder()
                .append(rectangle, otherPane.rectangle)
                .append(layout_pane_id, otherPane.layout_pane_id)
                .append(orientation, otherPane.orientation)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .append(rectangle)
                .append(layout_pane_id)
                .append(orientation)
                .toHashCode();
    }

    public Pane(int height, int width, int top_margin, int left_margin, int layout_pane_id, String orientation) {
        this.rectangle = new Rectangle(left_margin, top_margin, width, height);
        this.layout_pane_id = layout_pane_id;
        this.orientation = PlayerUtils.getOrientationType(orientation);
    }

    public Pane(int height, int width, int top_margin, int left_margin, int layout_pane_id, PlayerUtils.OrientationType orientation) {
        this.rectangle = new Rectangle(left_margin, top_margin, width, height);
        this.layout_pane_id = layout_pane_id;
        this.orientation = orientation;
    }

    public void setHeight(int height) {
        if (rectangle == null)
            rectangle = new Rectangle();
        rectangle.height = height;

    }

    public void setWidth(int width) {
        if (rectangle == null)
            rectangle = new Rectangle();
        rectangle.width = width;
    }

    public PlayerUtils.OrientationType getRequiredOrientation() {
        return orientation;
    }

    public void setOrientation(PlayerUtils.OrientationType orientation) {
        this.orientation = orientation;
    }

    public void setTop_margin(int top_margin) {
        if (rectangle == null)
            rectangle = new Rectangle();
        rectangle.y = top_margin;
    }

    public void setLeft_margin(int left_margin) {
        if (rectangle == null)
            rectangle = new Rectangle();
        rectangle.x = left_margin;
    }

    public void setRectangle(Rectangle rectangle) {
        this.rectangle = rectangle;
    }

    public void setLayout_pane_id(int layout_pane_id) {
        this.layout_pane_id = layout_pane_id;
    }

    public double getHeight() {
        return rectangle.getHeight();
    }


    public double getWidth() {
        return rectangle.getWidth();
    }

    public Rectangle getRectangle() {
        return rectangle;
    }

    public double getTop_margin() {
        return rectangle.getY();
    }


    public double getLeft_margin() {
        return rectangle.getX();
    }


    public int getLayout_pane_id() {
        return layout_pane_id;
    }


    public boolean isConflict(Pane activePane) {
        return rectangle.intersects(activePane.getRectangle());
    }


}
