package com.blynq.app.services;

import com.blynq.app.tools.ALogger;
import com.blynq.app.tools.StorageTools;

import org.apache.log4j.Logger;

import java.io.File;
import java.util.Date;
import java.util.List;


/**
 * Created by Jaydev on 18/05/16.
 */
public class Cleanup {
    private static Logger LOGGER = ALogger.getLogger(Cleanup.class);

    public static boolean deleteOldFiles(int thresholdDays, String dir) {
        boolean status = false;
        LOGGER.info("Deleting files older than " + thresholdDays + " from directory " + dir);
        if (new File(dir).exists())
            status = deleteOldFiles(thresholdDays, StorageTools.listFilesRec(dir));
        return status;
    }

    public static boolean deleteOldFiles(int thresholdDays, List<File> files) {
        boolean status = false;
        for (File file : files) {
            status = deleteIfOldFile(thresholdDays, file);
        }
        return status;
    }

    public static boolean deleteIfOldFile(int thresholdDays, File file) {
        if (!file.exists())
            return false;

        long diff = new Date().getTime() - file.lastModified();
        if (diff > thresholdDays * 24 * 60 * 60 * 1000) {
            file.delete();
            return true;
        }
        return false;
    }

    public static boolean deleteAllOldFiles(int keepFiles, String dirPath) {
        File dirFile = new File(dirPath);
        if (!dirFile.exists())
            return false;

        List<File> files = StorageTools.sortByLastModifiedDescending(dirPath);

        for (int i = keepFiles; i < files.size(); i++)
            files.get(i).delete();

        return true;

    }

}
