package com.blynq.app.fragments;

import android.app.Fragment;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.VideoView;

import com.blynq.app.activities.R;
import com.blynq.app.asyncs.MediaDownloadNotifier;
import com.blynq.app.asyncs.PDFPageChanger;
import com.blynq.app.helper.MediaHelper;
import com.blynq.app.model.Pane;
import com.blynq.app.model.PlayerUtils;
import com.blynq.app.model.PlaylistItem;
import com.blynq.app.properties.PlayerProperties;
import com.blynq.app.server.MediaDownloader;
import com.blynq.app.tools.ALogger;
import com.blynq.app.tools.BlurBuilder;
import com.blynq.app.tools.MediaOptimize;
import com.blynq.app.tools.MyWebChromeClient;
import com.blynq.app.tools.MyWebViewClient;
import com.blynq.app.tools.StorageTools;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerFragment;
import com.joanzapata.pdfview.PDFView;

import java.io.File;
import java.io.IOException;

/**
 * Created by Jaydev on 19/07/16.
 * Main Fragment, holder of elements in each pane
 */
public class PlayerPane extends Fragment implements YouTubePlayer.OnInitializedListener{

    public static org.apache.log4j.Logger LOGGER = ALogger.getLogger(PlayerPane.class);

    private static final String HEIGHT = "Height";
    private static final String WIDTH = "Width";
    private static final String LEFT_MARGIN = "left";
    private static final String RIGHT_MARGIN = "right";
    private static final String TOP_MARGIN = "top";
    private static final String BOTTOM_MARGIN = "bottom";
    private static final String REQUIRE_YOUTUBE = "require_youtube";
    private static final String REQUIRE_ROTATION = "require_rotation";

    private ImageView imageView;
    private ImageView gifView;
    private VideoView videoView;
    private WebView webView;
    private PDFView pdfView;
    private MediaPlayer audioPlayer;

    // initialised only if youtube player is necessary

    private boolean youTubePlayerNecessary = false;
    private LinearLayout youTubeContainer;
    private YouTubePlayer youTubePlayer = null;
    private boolean youtubePlayerInitFailed = false; // True if Player initialisation attempted but failed

    private boolean rotate;

    private Bitmap bitmap;
    private MediaDownloadNotifier mediaDownloadNotifier;
    private PDFPageChanger pdfPageChanger;

    private boolean logoMadeInvisible = false;


    /**
     * Taking 2nd parameter, as there is a strict limitation for only one active youtube player at once
     * @param pane
     * @param requiresYoutubePlayer
     * @return
     */
    public static PlayerPane newInstance(Pane pane, boolean requiresYoutubePlayer) {
        PlayerPane resizableFragment = new PlayerPane();
        Bundle args = new Bundle();

        int height, width, leftMarginPixels, rightMarginPixels, topMarginPixels, bottomMarginPixels;
        double leftMarginPercent, rightMarginPercent, topMarginPercent, bottomMarginPercent;

        boolean rotate = isRotationNecessary(pane.getRequiredOrientation());

        if (!rotate) {
            // Set Width Height dynamically
            height = (int) (PlayerProperties.screenHeight * pane.getHeight() / 100);
            width = (int) (PlayerProperties.screenWidth * pane.getWidth() / 100);

            leftMarginPercent = pane.getLeft_margin();
            rightMarginPercent = 100 - leftMarginPercent - pane.getWidth();
            topMarginPercent = pane.getTop_margin();
            bottomMarginPercent = 100 - topMarginPercent - pane.getHeight();

            leftMarginPixels = (int) leftMarginPercent * PlayerProperties.screenWidth / 100;
            rightMarginPixels = (int) rightMarginPercent * PlayerProperties.screenWidth / 100;
            topMarginPixels = (int) topMarginPercent * PlayerProperties.screenHeight / 100;
            bottomMarginPixels = (int) bottomMarginPercent * PlayerProperties.screenHeight / 100;
        }
        else {
            // Set Width Height dynamically
            height = (int) (PlayerProperties.screenWidth * pane.getHeight() / 100);
            width = (int) (PlayerProperties.screenHeight * pane.getWidth() / 100);

            double residue = (width - height) / 2;
            double mean = (width + height) / 2;

            leftMarginPixels = (int) (PlayerProperties.screenWidth - pane.getTop_margin() * PlayerProperties.screenWidth / 100 - mean);
            topMarginPixels = (int) (pane.getLeft_margin() * PlayerProperties.screenHeight / 100 + residue);

            rightMarginPixels = (int) (pane.getTop_margin() * PlayerProperties.screenWidth / 100 - residue);
            bottomMarginPixels = (int) (PlayerProperties.screenHeight - pane.getLeft_margin() * PlayerProperties.screenHeight / 100 - mean);
        }

        args.putInt(WIDTH, width);
        args.putInt(HEIGHT, height);

        args.putInt(LEFT_MARGIN, leftMarginPixels);
        args.putInt(RIGHT_MARGIN, rightMarginPixels);
        args.putInt(TOP_MARGIN, topMarginPixels);
        args.putInt(BOTTOM_MARGIN, bottomMarginPixels);
        args.putBoolean(REQUIRE_YOUTUBE, requiresYoutubePlayer);
        args.putBoolean(REQUIRE_ROTATION, rotate);

        LOGGER.info("width " + width + "  height " + height + " left " + leftMarginPixels
            + " right " + rightMarginPixels + " top " + topMarginPixels + " bottom " + bottomMarginPixels);

        resizableFragment.setArguments(args);
        return resizableFragment;
    }

    private static boolean isRotationNecessary(PlayerUtils.OrientationType requiredOrientationType) {
        PlayerUtils.OrientationType screenOrientationType =
                PlayerProperties.screenHeight <= PlayerProperties.screenWidth ? PlayerUtils.OrientationType.LANDSCAPE : PlayerUtils.OrientationType.PORTRAIT;
        return !requiredOrientationType.equals(screenOrientationType);
    }

    /**
     * Empty constructor for Android's internal use
     */
    public PlayerPane() {
    }

    public int dpToPx(int dp) {
        DisplayMetrics displayMetrics = getActivity().getResources().getDisplayMetrics();
        int px = Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
        return px;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_player_pane, container, false);
        FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) rootView.getLayoutParams();

        // set width height
        params.height = (getArguments().getInt(HEIGHT));
        params.width = (getArguments().getInt(WIDTH));

        // substitute parameters for left,top, right, bottom, (All 4 not necessary)
        params.setMargins((getArguments().getInt(LEFT_MARGIN)),
                (getArguments().getInt(TOP_MARGIN)),
                (getArguments().getInt(RIGHT_MARGIN)),
                (getArguments().getInt(BOTTOM_MARGIN)));

        youTubePlayerNecessary = getArguments().getBoolean(REQUIRE_YOUTUBE);
        rotate = getArguments().getBoolean(REQUIRE_ROTATION);
        rootView.setLayoutParams(params);

        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        imageView = (ImageView) getView().findViewById(R.id.imageView);
        gifView = (ImageView) getView().findViewById(R.id.gifImageView);
        videoView = (VideoView) getView().findViewById(R.id.videoView);
        webView = (WebView) getView().findViewById(R.id.webView);
        pdfView = (PDFView) getView().findViewById(R.id.pdfView);
        youTubeContainer = (LinearLayout) getView().findViewById(R.id.nested_fragment_container);

        if (rotate) {
            view.setRotation(90);
        }

        setFragmentDefaultSettings();

        if (youTubePlayerNecessary)
            addYouTubeFragment();
    }

    private void addYouTubeFragment() {
        YouTubePlayerFragment youTubePlayerFragment = YouTubePlayerFragment.newInstance();
        getChildFragmentManager().beginTransaction()
                .add(R.id.nested_fragment_container, youTubePlayerFragment, "youtube_fragment").commit();
        youTubePlayerFragment.initialize(PlayerProperties.youtubeDeveloperKey, this);
    }

    private void setFragmentDefaultSettings() {

        setWebViewDefaultSettings();
        setVideoViewDefaultSettings();

        audioPlayer = new MediaPlayer();
    }

    private void setVideoViewDefaultSettings() {
        videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                LOGGER.info("Video playing complete, moving to next playlist item - TODO"); // TODO
                // TODO - skip playlist item
            }
        });

        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mediaPlayer) {
                videoView.setBackgroundColor(Color.TRANSPARENT);
                videoView.setZOrderOnTop(true);
            }
        });

        videoView.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mediaPlayer, int i, int i1) {
                LOGGER.error("Error attempting to play video");
                setDefaultImage();
                // TODO - skip playlist item
                return true;
            }
        });

        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mediaPlayer) {
                mediaPlayer.setLooping(false); // TODO - should or should not loop ?
            }
        });
    }

    private void setWebViewDefaultSettings() {
        final WebSettings settings = webView.getSettings();

        settings.setJavaScriptEnabled(true);
        settings.setMediaPlaybackRequiresUserGesture(false);
        settings.setSupportMultipleWindows(false);
        settings.setJavaScriptCanOpenWindowsAutomatically(false);

        webView.setWebViewClient(new MyWebViewClient());
        webView.setWebChromeClient(new MyWebChromeClient());
    }


    public void setViewVisibility(PlayerUtils.MediaType mediaType) {
        switch (mediaType) {
            case IMAGE:
                videoView.setVisibility(View.INVISIBLE);
                pdfView.setVisibility(View.INVISIBLE);
                webView.setVisibility(View.INVISIBLE);
                gifView.setVisibility(View.INVISIBLE);
                youTubeContainer.setVisibility(View.INVISIBLE);
                imageView.setVisibility(View.VISIBLE);
                break;
            case AUDIO:
                pdfView.setVisibility(View.INVISIBLE);
                videoView.setVisibility(View.INVISIBLE);
                webView.setVisibility(View.INVISIBLE);
                gifView.setVisibility(View.INVISIBLE);
                youTubeContainer.setVisibility(View.INVISIBLE);
                imageView.setVisibility(View.VISIBLE);
                break;
            case VIDEO:
                pdfView.setVisibility(View.INVISIBLE);
                imageView.setVisibility(View.INVISIBLE);
                webView.setVisibility(View.INVISIBLE);
                gifView.setVisibility(View.INVISIBLE);
                youTubeContainer.setVisibility(View.INVISIBLE);
                videoView.setVisibility(View.VISIBLE);
                break;
            case PDF:
                videoView.setVisibility(View.INVISIBLE);
                imageView.setVisibility(View.INVISIBLE);
                webView.setVisibility(View.INVISIBLE);
                gifView.setVisibility(View.INVISIBLE);
                youTubeContainer.setVisibility(View.INVISIBLE);
                pdfView.setVisibility(View.VISIBLE);
                break;
            case GIF:
                videoView.setVisibility(View.INVISIBLE);
                imageView.setVisibility(View.INVISIBLE);
                pdfView.setVisibility(View.INVISIBLE);
                webView.setVisibility(View.INVISIBLE);
                youTubeContainer.setVisibility(View.INVISIBLE);
                gifView.setVisibility(View.VISIBLE);
                break;
            case WEB:
                videoView.setVisibility(View.INVISIBLE);
                imageView.setVisibility(View.INVISIBLE);
                pdfView.setVisibility(View.INVISIBLE);
                gifView.setVisibility(View.INVISIBLE);
                youTubeContainer.setVisibility(View.INVISIBLE);
                webView.setVisibility(View.VISIBLE);
                break;
            case YOUTUBE:
                videoView.setVisibility(View.INVISIBLE);
                imageView.setVisibility(View.INVISIBLE);
                pdfView.setVisibility(View.INVISIBLE);
                gifView.setVisibility(View.INVISIBLE);
                webView.setVisibility(View.INVISIBLE);
                youTubeContainer.setVisibility(View.VISIBLE);
                break;
            default:
                videoView.setVisibility(View.INVISIBLE);
                pdfView.setVisibility(View.INVISIBLE);
                webView.setVisibility(View.INVISIBLE);
                gifView.setVisibility(View.INVISIBLE);
                youTubeContainer.setVisibility(View.INVISIBLE);
                imageView.setVisibility(View.VISIBLE);  // defaulting to imageView

        }
    }

    public void changeActiveMedia(final PlaylistItem playlistItem) {

        String mediaPath = playlistItem.getLocalMediaPath();
        PlayerUtils.MediaType mediaType = playlistItem.getMedia_type();

        resetNotifiers();
        if (mediaPath != null && new File(mediaPath).exists()) {
            StorageTools.setLastModified(new File(mediaPath));

            switch (mediaType) {
                case IMAGE:
                    setViewVisibility(PlayerUtils.MediaType.IMAGE);
                    recycleBitmap();
                    Bitmap bitmap = MediaOptimize.reduceResolution(mediaPath, imageView.getWidth(), imageView.getHeight());
                    imageView.setImageBitmap(bitmap);
                    Bitmap blurredBackgroundBitmap = BlurBuilder.blur(getActivity(), bitmap);
                    imageView.setBackground(new BitmapDrawable( getResources(), blurredBackgroundBitmap ));
                    break;

                case VIDEO:
                    setViewVisibility(PlayerUtils.MediaType.VIDEO);
                    videoView.setVideoPath(mediaPath);
                    videoView.requestFocus();
                    videoView.start();
                    break;

                case AUDIO:
                    setViewVisibility(PlayerUtils.MediaType.AUDIO);
                    setDefaultImage();
                    try {
                        audioPlayer.setDataSource(mediaPath);
                        audioPlayer.prepare();
                        audioPlayer.start();
                    } catch (IOException e) {
                        LOGGER.error("Exception while attempting to play audio from file " + mediaPath, e);
                    }
                    break;

                case PDF:
                    setViewVisibility(PlayerUtils.MediaType.PDF);
                    pdfView.fromFile(new File(mediaPath)).defaultPage(1).enableSwipe(false).mask(Color.WHITE, 255).load();
                    int pageDisplayTime = playlistItem.getDisplay_time();
                    int pagesCount = MediaHelper.getNumOfPages(mediaPath);
                    // Async task to change page
                    pdfPageChanger = new PDFPageChanger(this);
                    pdfPageChanger.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, pageDisplayTime, pagesCount);
                    break;

                case GIF:
                    setViewVisibility(PlayerUtils.MediaType.GIF);
                    File gifFile = new File(mediaPath);
                    Glide.with(this).load(gifFile)
                            .override(gifView.getWidth(), gifView.getHeight())
                            .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                            .error(R.drawable.blynq_logo).into(gifView);
                    break;

                default:
                    setDefaultImage();
            }

        }
        else if (mediaType == PlayerUtils.MediaType.WEB) {
            setViewVisibility(PlayerUtils.MediaType.WEB);
            webView.loadUrl(playlistItem.getUrl());
        }
        else if (mediaType == PlayerUtils.MediaType.YOUTUBE){
            setViewVisibility(PlayerUtils.MediaType.YOUTUBE);
            if (youTubePlayer == null) {
                LOGGER.error("Youtube player is not initialised");
                setDefaultImage();
            }
            else {
                getActivity().findViewById(R.id.logo).setVisibility(View.INVISIBLE);
                logoMadeInvisible = true;
                youTubePlayer.loadVideo(PlayerUtils.getYouTubeId(playlistItem.getUrl()));
            }
        }
        else {
            LOGGER.info("File does not exist at path " + mediaPath + ", setting default image");
            setDefaultImage();
            if (mediaPath != null && mediaPath.trim().length() > 0) {
                if (!MediaDownloader.isDownloadPending(mediaPath)) {
                    // Valid media path, download not pending, file not present in local file system - try re-downloading
                    LOGGER.info("TODO - Triggering download again for " + playlistItem.getUrl());
                    if (MediaDownloader.localMediaItemRequired(playlistItem.getMedia_type()))
                        MediaDownloader.downloadMediaForPlaylistItem(playlistItem);
                }
                mediaDownloadNotifier = new MediaDownloadNotifier(this);
                // Async Task to check image periodically after 10 secs for image time
                mediaDownloadNotifier.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, playlistItem);
            }

        }


    }

    private void testAnimation() {
        RotateAnimation anim = new RotateAnimation(330f, 360f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        anim.setInterpolator(new LinearInterpolator());
//        anim.setRepeatCount(Animation.INFINITE);
        anim.setDuration(500);
        imageView.startAnimation(anim);


    }

    /**
     * remove bitmap from memory and invoke GC
     */
    private void recycleBitmap() {
        if (bitmap != null)
            bitmap.recycle();
        bitmap = null;
        System.gc();
    }

    public void resetNotifiers() {
        if (pdfPageChanger != null) {
            if (!pdfPageChanger.isCancelled())
                pdfPageChanger.cancel(true);
            pdfPageChanger = null;
        }

        if (mediaDownloadNotifier != null) {
            if (!mediaDownloadNotifier.isCancelled())
                mediaDownloadNotifier.cancel(true);
            mediaDownloadNotifier = null;
        }

        try {
            if (youTubePlayer != null && youTubePlayer.isPlaying()){
                youTubePlayer.pause();
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            youTubePlayer = null;
        }

        try {
            videoView.stopPlayback();

            audioPlayer.stop();
            audioPlayer.reset();

            if (logoMadeInvisible == true) {
                getActivity().findViewById(R.id.logo).setVisibility(View.VISIBLE);
                logoMadeInvisible = false;
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
    }

    public void setDefaultImage() {
        setViewVisibility(PlayerUtils.MediaType.IMAGE);
        imageView.setImageResource(R.drawable.blynq_logo);
    }

    public boolean changePdfPage(Integer pageNumber) {
        boolean status = false;
        if (pdfView.getVisibility() == View.VISIBLE) {
            LOGGER.info("PdfView is visible");
            pdfView.jumpTo(pageNumber);
            status = true;
        } else {
            LOGGER.debug("PdfView is NOT visible");
        }
        return status;
    }

    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean wasRestored) {
        this.youTubePlayer = youTubePlayer;
        youtubePlayerInitFailed = false;
        youTubePlayer.setPlayerStateChangeListener(playerStateChangeListener);
    }

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {
        youTubePlayer = null;
        youtubePlayerInitFailed = true;
        String errorMessage = String.format(
                "Error initializing youtube player", youTubeInitializationResult.toString());
        LOGGER.error(errorMessage);
    }

    @Override
    public void onDestroyView() {
        if(youTubePlayer!=null) {
            try {
                LOGGER.info("Releasing Youtube player");
                youTubePlayer.release();
                youTubePlayer = null;
            }
            catch (Exception ex) {
                LOGGER.error("Youtube player released already, must be unreachable code, developer to verify");
            }
        }
        super.onDestroyView();
    }

    public boolean isReady() {
        boolean youtubePlayerSetupReady = (youTubePlayerNecessary && youTubePlayer != null) //youtube player must be initialised by now
                || (youTubePlayerNecessary && youtubePlayerInitFailed) // initialization failed
                || (!youTubePlayerNecessary); // youtube player not required

        return isVisible() && youtubePlayerSetupReady ;
    }

    private YouTubePlayer.PlayerStateChangeListener playerStateChangeListener = new YouTubePlayer.PlayerStateChangeListener() {
        @Override
        public void onLoading() {

        }

        @Override
        public void onLoaded(String s) {

        }

        @Override
        public void onAdStarted() {

        }

        @Override
        public void onVideoStarted() {

        }

        @Override
        public void onVideoEnded() {
            LOGGER.info("Youtube player ended, looping");
            youTubePlayer.play();
        }

        @Override
        public void onError(YouTubePlayer.ErrorReason errorReason) {
            LOGGER.error( "Youtube player unable to play from given url - " + errorReason );
            setDefaultImage();
        }
    };

    public void removeSubFragments() {
        if (youTubePlayerNecessary && !youtubePlayerInitFailed){
            Fragment fragmentToBeRemoved = getChildFragmentManager().findFragmentByTag("youtube_fragment");
            if (fragmentToBeRemoved != null)
                getChildFragmentManager().beginTransaction().remove(fragmentToBeRemoved).commit();
        }
    }
}
