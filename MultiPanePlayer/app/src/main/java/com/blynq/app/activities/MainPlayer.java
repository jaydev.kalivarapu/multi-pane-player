package com.blynq.app.activities;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;

import com.blynq.app.asyncs.PanePlayerAsyncTask;
import com.blynq.app.fragments.PlayerPane;
import com.blynq.app.model.Campaign;
import com.blynq.app.model.PlayerUtils;
import com.blynq.app.properties.PlayerProperties;
import com.blynq.app.services.CampaignProducer;
import com.blynq.app.services.Cleanup;
import com.blynq.app.tools.ALogger;
import com.blynq.app.tools.StorageTools;

import org.apache.log4j.Logger;
import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class MainPlayer extends FragmentActivity {

    private static Logger LOGGER;
    private CampaignProducer campaignProducer;
    private ScheduledExecutorService worker;
    private Runnable poller;
    private Runnable cleaner;
    private UpdateMainView consumer;
    private boolean playerRunStatus = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFormat(PixelFormat.TRANSLUCENT); // to stop videoview flickering
        setContentView(R.layout.activity_main_player);

        setActivityDefaultConfiguration();
        initializeWorkerTasks();

        // Launch Async Task to consume Queue
        consumer = new UpdateMainView();
        consumer.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private void initializeWorkerTasks() {

        worker = Executors.newSingleThreadScheduledExecutor();

        poller = new Runnable() {
            public void run() {
                try {
                    campaignProducer.pollServer();
                } catch (IOException | JSONException e) {
                    LOGGER.error("Exception while polling server for campaign details, check internet connection or server connection \n", e);
                }
            }
        };

        cleaner = new Runnable() {
            @Override
            public void run() {
                Cleanup.deleteAllOldFiles(2, StorageTools.getAbsoluteDirectoryPath(PlayerProperties.serFolder));    // Keep only latest 2 files
                StorageTools.maintainMemoryEmptyThreshold(25); // maintain atleast 25% free
                Cleanup.deleteOldFiles(6, StorageTools.getAbsoluteDirectoryPath(PlayerProperties.logsFolder));
            }
        };


        worker.scheduleAtFixedRate(poller, 0, //initial delay in secs
                PlayerProperties.schedulePollPeriodicity, // run every x seconds
                TimeUnit.SECONDS);

        worker.scheduleAtFixedRate(cleaner, 0,
                24,              // execute once every day
                TimeUnit.HOURS); // clean immediately

    }

    /**
     * All necessary default initialization and layout configurations
     */
    private void setActivityDefaultConfiguration() {
        LOGGER = ALogger.getLogger(MainPlayer.class);
        LOGGER.info("Setting default properties and configurations");

        hideSystemUI();
        Thread.setDefaultUncaughtExceptionHandler(onCrashRestart);
        // Initializing global setup
        PlayerUtils.fragmentManager = getFragmentManager();
        PlayerUtils.activePanes = new ArrayList<>();
//        PlayerUtils.setDefaultCampaign();
        campaignProducer = new CampaignProducer(StorageTools.getDeserCampaignPriorityQueue());
    }


    private class UpdateMainView extends AsyncTask<Void, Campaign, String> {

        @Override
        protected String doInBackground(Void... voids) {

            Campaign currCampaign;

            while (playerRunStatus) {
                final PriorityBlockingQueue<Campaign> campaignQueue = campaignProducer.getCampaignPriorityQueue().getCampaignQueue();
                currCampaign = campaignQueue.peek();
                if ((currCampaign != null && currCampaign.isExpired()) || PlayerUtils.isActive(currCampaign)) {
                    LOGGER.info("Discarding expired campaign with id - " + currCampaign.getSchedule_id());
                    campaignQueue.poll();
                }
                else if (currCampaign != null && currCampaign.isQualified()) {
                    LOGGER.info("Playing campaign with id " + currCampaign.getSchedule_id());
                    campaignQueue.poll();
                    updateLayout(currCampaign);
                    LOGGER.info("Campaign " + currCampaign.getSchedule_id() + " launched !!");
                }
                else {
                    // Waiting before next check
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        LOGGER.error("Exception while waiting before playing next qualified campaign");
                    }
                    refreshLayout();
                }
            }

            return "COMPLETE";
        }

        private void refreshLayout() {
            // Find expired and dirty campaigns, throw them away. If active campaigns are none - set default
            LOGGER.debug("Refreshing layout ..");
            List<PanePlayerAsyncTask> campaignsToBeRemoved = new ArrayList<>();
            for (PanePlayerAsyncTask panePlayerAsyncTask: PlayerUtils.activePanes) {
                if (panePlayerAsyncTask.getCampaign().isExpired() || panePlayerAsyncTask.isDirty())
                    campaignsToBeRemoved.add(panePlayerAsyncTask);
            }
            PlayerUtils.removeFragments(campaignsToBeRemoved);
//            if (PlayerUtils.activePanes.size() == 0 && PlayerUtils.isDefaultLayoutSet == false)
//                updateLayout(PlayerProperties.defaultCampaign);
        }

        private void updateLayout(Campaign currCampaign) {
            Thread.currentThread().setPriority(Thread.MAX_PRIORITY); // increasing thread priority
            List<PanePlayerAsyncTask> conflictingPanes = PlayerUtils.getConflictingFragmentsList(currCampaign);
            if (conflictingPanes != null && conflictingPanes.size() > 0) {
                LOGGER.info("Campaign " + currCampaign + " has " + conflictingPanes.size() + " conflicting panes");
                PlayerUtils.removeFragments(conflictingPanes);
            }
            publishProgress(currCampaign);
            Thread.currentThread().setPriority(Thread.NORM_PRIORITY); // setting back to normal
        }

        @Override
        protected void onProgressUpdate(Campaign... campaigns) {
            Thread.currentThread().setPriority(Thread.MAX_PRIORITY); // increasing thread priority
            // Launching Async Task to consume campaign
            String fragmentName = PlayerUtils.getFragmentName(campaigns[0].getPane());
            final PlayerPane playerPane = PlayerPane.newInstance(
                    campaigns[0].getPane(), campaigns[0].containsYoutubePlaylistItem());
//            PlayerUtils.isDefaultLayoutSet = (campaigns[0].equals(PlayerProperties.defaultCampaign)) ? false : true;
            PlayerUtils.addPaneToScreen(playerPane, fragmentName);
            PanePlayerAsyncTask panePlayerAsyncTask = new PanePlayerAsyncTask(playerPane, campaigns[0]);
            panePlayerAsyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            PlayerUtils.activePanes.add(panePlayerAsyncTask);
            Thread.currentThread().setPriority(Thread.NORM_PRIORITY); // setting back to normal
        }
    }


    /**
     * Get app to full screen hiding all toolbars
     */
    private void hideSystemUI() {
        View decorView = getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
                | View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
                | View.SYSTEM_UI_FLAG_IMMERSIVE;
        decorView.setSystemUiVisibility(uiOptions);
    }


    // TODO - Incomplete
    @Override
    protected void onPause() {
        super.onPause();
        LOGGER.debug("Destroying instance and related worker threads");
        worker.shutdown();
        if (consumer != null) {
            consumer.cancel(true);
            consumer = null;
        }
        finish();
    }

    // Restart app if crashes
    private Thread.UncaughtExceptionHandler onCrashRestart = new Thread.UncaughtExceptionHandler() {
        public void uncaughtException(Thread thread, Throwable ex) {

            LOGGER.error("Unhandled Runtime exception occured ", ex);
            LOGGER.info("Restarting app in 5 seconds");
            Intent crashedIntent = new Intent(getApplicationContext(), RegisterActivity.class);
            PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0, crashedIntent, 0);

            AlarmManager mgr = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
            mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 5000, pendingIntent);
            System.exit(2);
        }
    };

}
