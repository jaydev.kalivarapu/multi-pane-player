package com.blynq.app.server;

import com.blynq.app.tools.ALogger;

import org.apache.log4j.Logger;


/**
 * Created by Jaydev on 13/08/16.
 */
public class DownloaderRunnable implements Runnable {

    private static Logger LOGGER = ALogger.getLogger(DownloaderRunnable.class);
    String url, tempFolder, targetFolder, fileName;

    public DownloaderRunnable(String url, String tempFolder, String targetFolder, String fileName) {
        this.url = url;
        this.tempFolder = tempFolder;
        this.targetFolder = targetFolder;
        this.fileName = fileName;
    }

    @Override
    public void run() {
        long startTime = System.nanoTime();
        boolean status = MediaDownloader.downloadFile(url, tempFolder, targetFolder, fileName);
        long endTime = System.nanoTime();
        if (status)
            LOGGER.info("Downloaded from url " + url + " in " + (endTime - startTime)/1000000000 + " seconds" );
        else
            LOGGER.error("Download failed from " + url);
    }
}
