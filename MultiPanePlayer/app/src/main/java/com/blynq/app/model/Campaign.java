package com.blynq.app.model;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Jaydev on 07/05/16.
 */
public class Campaign implements Comparable, Serializable {

    private int schedule_id;
    private Date start_time;
    private List<Playlist> playlists;
    private Date last_updated_time;
    private Date end_time;
    private Pane pane;


    public Campaign(int schedule_id, List<Playlist> playlists, Date last_updated_time, Date start_time, Date end_time, Pane pane) {
        this.schedule_id = schedule_id;
        this.playlists = playlists;
        this.last_updated_time = last_updated_time;
        this.start_time = start_time; // Converting UTC to IST (All inputs are given in UTC)
        this.end_time = end_time;
        this.pane = pane;
    }

    @Override
    public boolean equals(Object other) {
        if (other == null) return false;
        if (other == this) return true;
        if (!(other instanceof Campaign)) return false;
        Campaign otherCampaign = (Campaign) other;

        return new EqualsBuilder()
                .append(schedule_id, otherCampaign.schedule_id)
                .append(playlists, otherCampaign.playlists)
                .append(end_time, otherCampaign.end_time)
                .append(pane, otherCampaign.pane)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 31)
                .append(schedule_id)
                .append(playlists)
                .append(end_time)
                .append(pane)
                .toHashCode();
    }

    public Campaign() {
    }

    public int getSchedule_id() {
        return schedule_id;
    }

    public void setSchedule_id(int schedule_id) {
        this.schedule_id = schedule_id;
    }

    public Date getLast_updated_time() {
        return last_updated_time;
    }

    public void setLast_updated_time(Date last_updated_time) {
        this.last_updated_time = last_updated_time;
    }

    public Date getStart_time() {
        return start_time;
    }

    public List<Playlist> getPlaylists() {
        return playlists;
    }


    public Date getEnd_time() {
        return end_time;
    }

    @Override
    public String toString() {
        return "schedule_id = " + schedule_id + "   Playlists : " + playlists + "   start_time : " + start_time
                + "   end_time : " + end_time;
    }

    @Override
    public int compareTo(Object o) {
        return this.getStart_time().compareTo(((Campaign) o).getStart_time());
    }

    public int getPlaylistListLoopTime() {
        int totalTimeInSec = 0;
        for (Playlist playlist : playlists)
            totalTimeInSec += playlist.getPlaylistLoopTime();
        return totalTimeInSec;
    }

    public void setEnd_time(Date end_time) {
        this.end_time = end_time;
    }

    public void setStart_time(Date start_time) {
        this.start_time = start_time;
    }

    public Pane getPane() {
        return pane;
    }

    public void setPane(Pane pane) {
        this.pane = pane;
    }

    public boolean isValidCampaign() {
        if (start_time.compareTo(end_time) < 0 && playlists.size() > 0 && Playlist.isValidPlaylistList(playlists))
            return true;

        return false;
    }

    public Campaign getValidatedCampaign() {
        final List<Playlist> validatedPlaylistList = Playlist.getValidatedPlaylistList(playlists);
        Campaign validatedCampaign;
        if (validatedPlaylistList != null && validatedPlaylistList.size() > 0) {
            validatedCampaign = new Campaign(schedule_id, validatedPlaylistList,
                    last_updated_time, start_time, end_time, pane);
            return validatedCampaign;
        } else
            return null;
    }

    public static List<Campaign> getValidatedCampaignList(List<Campaign> campaignList) {
        List<Campaign> validCampaignsList = new ArrayList<>();
        for (Campaign campaign : campaignList) {
            if (campaign.isValidCampaign()) {
                System.out.println("Valid campaign - " + campaign.getSchedule_id());
                validCampaignsList.add(campaign);
            } else {
                System.out.println("Invalid campaign - " + campaign.getSchedule_id());
                Campaign validCampaign = campaign.getValidatedCampaign();
                if (validCampaign != null && validCampaign.playlists.size() > 0)
                    validCampaignsList.add(validCampaign);
            }
        }
        return validCampaignsList;
    }

    public boolean isExpired() {
        return getStart_time().compareTo(new Date()) <= 0 && getEnd_time().compareTo(new Date()) <= 0;
    }

    public boolean isQualified() {
        return getPlaylists().size() > 0 && getStart_time().compareTo(new Date()) <= 0 && getEnd_time().compareTo(new Date()) > 0;
    }

    public static List<Campaign> getQualifiedCampaignsForCurrentTime(List<Campaign> campaignList) {
        List<Campaign> currentlyQualifiedCampaigns = new ArrayList<>();
        for (Campaign campaign: campaignList) {
            if (campaign.isExpired())
                continue;
            else if (!campaign.isQualified()) {
                break;
            }
            currentlyQualifiedCampaigns.add(campaign);
        }
        return currentlyQualifiedCampaigns;
    }

    public boolean containsYoutubePlaylistItem () {
        for (Playlist playlist: playlists)
            for (PlaylistItem playlistItem: playlist.getPlaylist_items())
                if (playlistItem.getMedia_type() != null && playlistItem.getMedia_type().equals(PlayerUtils.MediaType.YOUTUBE))
                    return true;
        return false;
    }
}
