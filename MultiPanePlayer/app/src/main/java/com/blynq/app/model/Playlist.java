package com.blynq.app.model;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jaydev on 07/05/16.
 */
public class Playlist implements Serializable {
    private List<PlaylistItem> playlist_items;
    private int playlist_id;
    private String playlist_title;

    public Playlist(List<PlaylistItem> playlist_items, int playlist_id, String playlist_title) {
        this.playlist_items = playlist_items;
        this.playlist_id = playlist_id;
        this.playlist_title = playlist_title;
    }

    public Playlist() {
    }

    @Override
    public boolean equals(Object other) {
        if (other == null) return false;
        if (other == this) return true;
        if (!(other instanceof Playlist)) return false;

        Playlist otherPlaylist = (Playlist) other;
        return new EqualsBuilder()
                .append(playlist_items, otherPlaylist.playlist_items)
                .append(playlist_id, otherPlaylist.playlist_id)
                .append(playlist_title, otherPlaylist.playlist_title)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 31)
                .append(playlist_items)
                .append(playlist_id)
                .append(playlist_title)
                .toHashCode();
    }

    public List<PlaylistItem> getPlaylist_items() {
        return playlist_items;
    }

    public void setPlaylist_items(List<PlaylistItem> playlist_items) {
        this.playlist_items = playlist_items;
    }

    public void addPlaylistItem(PlaylistItem playlistItem) {
        if (playlistItem.isValidPlaylistItem())
            playlist_items.add(playlistItem);
    }

    public int getPlaylist_id() {
        return playlist_id;
    }

    public void setPlaylist_id(int playlist_id) {
        this.playlist_id = playlist_id;
    }

    public String getPlaylist_title() {
        return playlist_title;
    }

    public void setPlaylist_title(String playlist_title) {
        this.playlist_title = playlist_title;
    }

    public int getPlaylistLoopTime() {
        int totalTimeInSec = 0;
        for (PlaylistItem playlistItem : playlist_items) {
            totalTimeInSec += playlistItem.getDisplay_time();
        }

        return totalTimeInSec;
    }

    private boolean isValidPlaylist() {
        if (playlist_items.size() <= 0)
            return false;

        for (PlaylistItem playlistItem : playlist_items)
            if (!playlistItem.isValidPlaylistItem())
                return false;
        return true;
    }

    public static boolean isValidPlaylistList(List<Playlist> playlists) {

        if (playlists.size() <= 0)
            return false;

        for (Playlist playlist : playlists) {
            if (!playlist.isValidPlaylist())
                return false;
        }
        return true;
    }


    public Playlist getValidatedPlaylist() {
        List<PlaylistItem> validPlaylistItems = PlaylistItem.getValidPlaylistItems(playlist_items);

        if (validPlaylistItems != null && validPlaylistItems.size() > 0)
            return new Playlist(validPlaylistItems, playlist_id, playlist_title);
        else
            return null;
    }

    public static List<Playlist> getValidatedPlaylistList(List<Playlist> playlists) {
        List<Playlist> validPlaylists = new ArrayList<>();

        for (Playlist playlist : playlists) {
            Playlist validatedPlaylist = playlist.getValidatedPlaylist();

            if (validatedPlaylist != null && validatedPlaylist.getPlaylist_items().size() > 0)
                validPlaylists.add(validatedPlaylist);
        }

        if (validPlaylists.size() > 0)
            return validPlaylists;
        else
            return null;
    }
}
