package com.blynq.app.asyncs;

import android.os.AsyncTask;

import com.blynq.app.fragments.PlayerPane;
import com.blynq.app.tools.ALogger;

import org.apache.log4j.Logger;


/**
 * Created by Jaydev on 26/07/16.
 */
public class PDFPageChanger extends AsyncTask<Integer, Integer, Void> {

    private Logger LOGGER = ALogger.getLogger(PDFPageChanger.class);
    private PlayerPane playerPane;

    public PDFPageChanger(PlayerPane playerPane) {
        this.playerPane = playerPane;
    }

    @Override
    protected Void doInBackground(Integer... integers) {
        Integer pagePlayTime = integers[0];
        Integer numberOfPages = integers[1];

        try {
            for (int pageNumber = 2; pageNumber <= numberOfPages; pageNumber++) {
                Thread.sleep(1000 * pagePlayTime);
                if (isCancelled())
                    break;

                LOGGER.info("Setting page " + pageNumber + " of " + numberOfPages);
                publishProgress(pageNumber);
            }

        } catch (InterruptedException e) {
            LOGGER.error("Interrupt to PDFPageChanger");
            cancel(true);
        }

        return null;
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        playerPane.changePdfPage(values[0]);

    }
}
