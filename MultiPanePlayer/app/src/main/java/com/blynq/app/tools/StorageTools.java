package com.blynq.app.tools;

import android.os.Environment;
import android.os.StatFs;
import android.util.Log;

import com.blynq.app.model.Campaign;
import com.blynq.app.model.CampaignPriorityQueue;
import com.blynq.app.properties.PlayerProperties;
import com.blynq.app.server.MediaDownloader;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;


/**
 * Created by Jaydev on 21/05/16.
 */
public class StorageTools {
    public static Logger LOGGER = ALogger.getLogger(ObjectState.class);

    public static String getFileExtension(File file) {
        String name = file.getName();
        try {
            return name.substring(name.lastIndexOf(".") + 1).trim();
        } catch (Exception e) {
            return "";
        }
    }

    public static String getFileExtension(String fileName) {
        try {
            return fileName.substring(fileName.lastIndexOf(".") + 1).trim();
        } catch (Exception e) {
            return "";
        }
    }

    public static String getAbsoluteDirectoryPath(String relativeDirPath) {
        File absoluteDirFile = new File(Environment.getExternalStorageDirectory(), relativeDirPath);
        if (absoluteDirFile.exists()) {
            return absoluteDirFile.getAbsolutePath();
        }
        return relativeDirPath;
    }

    /**
     * Returns latest file from directory with particular extension
     *
     * @param dirPath
     * @param extension
     * @return
     */
    public static String getLatestFilefromDir(String dirPath, final String extension) {
        LOGGER.info("Reading directory " + dirPath + " for files with extension " + extension);
        File dir = new File(dirPath);

        if (!dir.exists())
            return null;

        File[] files = dir.listFiles(new FilenameFilter() {
            public boolean accept(File dir, String name) {
                return name.toLowerCase().endsWith(extension);
            }
        });

        if (files == null || files.length == 0)
            return null;

        File lastModifiedFile = files[0];
        for (int i = 1; i < files.length; i++) {
            if (lastModifiedFile.lastModified() < files[i].lastModified()) {
                lastModifiedFile = files[i];
            }
        }
        return lastModifiedFile.getAbsolutePath();
    }

    public static File getLatestFilefromDir(String dirPath){
        File dir = new File(dirPath);
        File[] files = dir.listFiles();
        if (files == null || files.length == 0) {
            return null;
        }

        File lastModifiedFile = files[0];
        for (int i = 1; i < files.length; i++) {
            if (lastModifiedFile.lastModified() < files[i].lastModified()) {
                lastModifiedFile = files[i];
            }
        }
        return lastModifiedFile;
    }

    public static void createDirsIfNotExists(String[] paths) {
        for (String path : paths)
            createDirIfNotExists(path);
    }

    public static boolean createDirIfNotExists(String path) {
        boolean success = true;
        File file = new File(Environment.getExternalStorageDirectory(), path);
        if (!file.exists()) {
            if (!file.mkdirs()) {
                Log.e("TravellerLog :: ", "Problem creating folder " + path);
                success = false;
            }
        }
        return success;
    }

    /**
     * Traverses the directory recursively to list all files
     *
     * @param directoryName
     * @return
     */
    public static List<File> listFilesRec(String directoryName) {
        File directory = new File(directoryName);

        List<File> resultList = new ArrayList<File>();

        if (!directory.exists())
            return resultList;

        File[] fList = directory.listFiles();
        resultList.addAll(Arrays.asList(fList));

        for (File file : fList) {
            if (file.isDirectory()) {
                resultList.addAll(listFilesRec(file.getAbsolutePath()));
                resultList.remove(file);
            }
        }
        return resultList;
    }

    public static boolean deleteRecursive(File pathToFolder) {
        boolean status = false;
        File[] c = pathToFolder.listFiles();
        for (File file : c) {
            if (file.isDirectory()) {
                deleteRecursive(file);
                file.delete();
            } else {
                file.delete();
            }
            status = true;
        }
        pathToFolder.delete();
        return status;
    }

    /**
     *  Creates necessary Directory structure for player
     */
    public static void verifyPlayerDirectoryStructure() {
        String[] paths = {
                PlayerProperties.imagesFolder,
                PlayerProperties.videosFolder,
                PlayerProperties.pdfFolder,
                PlayerProperties.gifFolder,
                PlayerProperties.webFolder,
                PlayerProperties.webFolder,
                PlayerProperties.othersFolder,
                PlayerProperties.webFolder,
                PlayerProperties.othersFolder,
                PlayerProperties.defaultFolder,
                PlayerProperties.serFolder,
                PlayerProperties.tempFolder};

        createDirsIfNotExists(paths);
    }

    /**
     * Returns false if move is unsuccessful due to non-existence of src or dest files/directories
     *
     * @param srcDir
     * @param destDir
     * @param fileName
     * @return
     */
    public static boolean moveFile(String srcDir, String destDir, String fileName) {
        File srcFile = new File(srcDir, fileName);
        File destDirFile = new File(destDir);
        if (srcFile.exists() && destDirFile.exists()) {
            srcFile.renameTo(new File(destDir, fileName));
            LOGGER.debug("File" + fileName + " moved from " + srcDir + " to " + destDir);
            return true;
        } else {
            LOGGER.error("Either " + srcFile.getAbsolutePath() + " or " + destDirFile.getAbsolutePath() + " do not exist");
            return false;
        }
    }

    public static boolean clearTempFolder() {
        String tempFolderPath = PlayerProperties.tempFolder;
        try {
            FileUtils.cleanDirectory(new File(Environment.getExternalStorageDirectory(), tempFolderPath));
            return true;
        } catch (IOException e) {
            LOGGER.error("IOException while attempting to access temp folder");
        } catch (IllegalArgumentException e) {
            LOGGER.error("Folder" + tempFolderPath + "does not exist");
        } catch (Exception e) {
            LOGGER.error("Unknown Exception while attempting to access temp folder");
        }
        return false;
    }

    public static boolean setLastModified(File file) {
        try {
            RandomAccessFile raf = new RandomAccessFile(file, "rw");
            long length = raf.length();
            raf.setLength(length + 1);
            raf.setLength(length);
            raf.close();
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    public static void deleteFilesInFolder(File fileOrDirectory) {
        if (fileOrDirectory.isDirectory())
            for (File child : fileOrDirectory.listFiles()) {
                if (child.isDirectory())
                    deleteRecursive(child);
                else
                    child.delete();
            }
    }

    public static void deleteFilesInFolder(String pathToDir) {
        File dirFile = new File(pathToDir);
        if (dirFile.exists()) {
            deleteFilesInFolder(dirFile);
        }
    }

    public static long removeLeastRecentlyUsedFile() {
        String leastRecentlyUsedFilePath = getLeastRecentlyUsedFilePath();
        long sizeEmptied = 0;
        if (leastRecentlyUsedFilePath != null && leastRecentlyUsedFilePath.length() > 0) {
            File leastRecentlyUsedFile = new File(leastRecentlyUsedFilePath);
            sizeEmptied = leastRecentlyUsedFile.length();
            leastRecentlyUsedFile.delete();
        }
        return sizeEmptied;
    }

    private static String getLeastRecentlyUsedFilePath() {
        return null;
    }

    /**
     * Ascending order sorting of files in directory
     * @param dirPath
     * @return
     */
    public static List<File> sortByLastModifiedAscending(String dirPath) {
        List<File> files = listFilesRec(dirPath);
        Collections.sort(files, new Comparator<File>() {
            public int compare(File o1, File o2) {
                return Long.compare(o1.lastModified(), o2.lastModified());
            }
        });
        return files;
    }

    /**
     * Descending order sorting of files in directory
     * @param dirPath
     * @return
     */
    public static List<File> sortByLastModifiedDescending(String dirPath) {
        List<File> files = listFilesRec(dirPath);
        Collections.sort(files, new Comparator<File>() {
            public int compare(File o1, File o2) {
                return Long.compare(o2.lastModified(), o1.lastModified());
            }
        });
        return files;
    }

    public static float percentageAvailableMemory() {
        StatFs statFs = new StatFs(Environment.getExternalStorageDirectory().getAbsolutePath());
        long blockSize = statFs.getBlockSizeLong();
        long totalSize = statFs.getBlockCountLong()*blockSize;
        long availableSize = statFs.getAvailableBlocksLong()*blockSize;
        long freeSize = statFs.getFreeBlocksLong()*blockSize;

        return (float) 100 * freeSize * 1.0f / totalSize;
    }

    public static void maintainMemoryEmptyThreshold(float percentageEmptyLevel) {
        List<File> files = sortByLastModifiedAscending(Environment.getExternalStorageDirectory() + "/" + PlayerProperties.mediaFolder);
        int i =0;

        while (percentageAvailableMemory() < percentageEmptyLevel && files.size() > i+1) {
                files.get(i).delete();
                i++;
                LOGGER.info("Removing file " + files.get(i).getAbsolutePath() + " to empty space");
        }
    }

    /**
     * Fetches latest available de-serialized version
     *
     * @return
     */
    public static CampaignPriorityQueue getDeserCampaignPriorityQueue() {
        CampaignPriorityQueue campaignPriorityQueue = null;
        try {

            File serFolder = new File(StorageTools.getAbsoluteDirectoryPath(PlayerProperties.serFolder));
            String absSerFilePath = StorageTools.getLatestFilefromDir(serFolder.getAbsolutePath(), ".ser");
            Object deserObj = ObjectState.deserializeObject(absSerFilePath);
            if (deserObj != null) {
                campaignPriorityQueue = (CampaignPriorityQueue) deserObj;
                final Object[] campaignArray = campaignPriorityQueue.getCampaignQueue().toArray();
                final List<Campaign> campaignList =
                        Arrays.asList(Arrays.copyOf(campaignArray, campaignArray.length, Campaign[].class));
                MediaDownloader.downloadAllMedia(campaignList);  // TODO - optimize download requirement as newly updated schedule may override
            }
        } catch (Exception e) {
            LOGGER.error("No earlier state available or latest available state is corrupt !! \n" +
                    "Exception message - ", e);
        }
        return campaignPriorityQueue;
    }

    public static void main(String[] args) throws ClassNotFoundException, NoSuchMethodException, InvocationTargetException, IllegalAccessException, NoSuchFieldException {
        File dir = new File("/Users/Jaydev/Desktop/BlYnQ/test/");
        List<File> files = sortByLastModifiedAscending(dir.getAbsolutePath());

        for (File file: files) {
            System.out.println(file.lastModified());
        }
    }
}
