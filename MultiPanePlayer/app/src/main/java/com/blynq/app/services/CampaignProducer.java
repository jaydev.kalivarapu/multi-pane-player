package com.blynq.app.services;

import android.os.Environment;

import com.blynq.app.model.Campaign;
import com.blynq.app.model.CampaignPriorityQueue;
import com.blynq.app.model.PlayerUtils;
import com.blynq.app.properties.PlayerProperties;
import com.blynq.app.server.MediaDownloader;
import com.blynq.app.server.ServerComm;
import com.blynq.app.tools.ALogger;
import com.blynq.app.tools.ObjectState;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.PriorityBlockingQueue;


/**
 * Created by Jaydev on 07/05/16.
 */
public class CampaignProducer {

    private static Logger LOGGER = ALogger.getLogger(CampaignProducer.class);
    private CampaignPriorityQueue campaignPriorityQueue;
    ObjectMapper objectMapper;

    public CampaignProducer() {
        campaignPriorityQueue = new CampaignPriorityQueue(10);
        configureObjectMapper();
    }

    public CampaignProducer(CampaignPriorityQueue campaignPriorityQueue) {
        if (campaignPriorityQueue != null)
            this.campaignPriorityQueue = campaignPriorityQueue;
        else
            this.campaignPriorityQueue = new CampaignPriorityQueue(10);

        configureObjectMapper();
    }

    private void configureObjectMapper() {
        objectMapper = new ObjectMapper();
        objectMapper.configure(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES, true);
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    }

    /**
     * Add new element to existing queue
     *
     * @param newCampaign
     */
    private void addElement(Campaign newCampaign) {

        // Inconsistent time stamps
        if (newCampaign.getStart_time().compareTo(newCampaign.getEnd_time()) >= 0)
            return;

        LOGGER.info("Adding campaign element " + newCampaign.getSchedule_id());
        campaignPriorityQueue.getCampaignQueue().add(newCampaign);
    }

    /**
     * Serialize after adding multiple campaign elements
     *
     * @param campaigns
     */
    public void replaceCampaigns(List<Campaign> campaigns, Date newReceivedDate) {

        // Everytime entire list is retrieved
        campaignPriorityQueue.setCampaignQueue(new PriorityBlockingQueue<Campaign>(10));

        for (Campaign campaign : campaigns)
            addElement(campaign);

        LOGGER.info("Updating last_received_date from " +
                campaignPriorityQueue.getLastModified() + " to " + newReceivedDate);
        campaignPriorityQueue.setLastModified(newReceivedDate);

        String serFileName = new SimpleDateFormat("yyyy-MM-dd hh-mm-ss'.ser'").format(new Date());
        File absoluteSerFolder = new File(Environment.getExternalStorageDirectory(), PlayerProperties.serFolder);
        String absoluteSerFileName = new File(absoluteSerFolder, serFileName).getAbsolutePath();
        ObjectState.serializeObject(campaignPriorityQueue, absoluteSerFileName);

        // Notify that queue is updated

    }


    public CampaignPriorityQueue getCampaignPriorityQueue() {
        return campaignPriorityQueue;
    }

    public void pollServer() throws IOException, JSONException {

        String url = PlayerProperties.baseUrl + PlayerProperties.schedulesUrlAppender;
        Map<String, String> jsonEntries = new HashMap<>();
        jsonEntries.put("device_key", PlayerProperties.androidId);
        final String lastModifiedDateString = PlayerUtils.getDateString(campaignPriorityQueue.getLastModified());

        jsonEntries.put("last_received", lastModifiedDateString);
        final String jsonRequestString = new JSONObject(jsonEntries).toString();
        LOGGER.info("Polling server for new campaigns at URL " + url + " ..." + " request - " + jsonRequestString);

        JSONObject jsonResponse = ServerComm.postToServer(url, jsonRequestString);
        LOGGER.info("Request - " + jsonRequestString + "\nResponse from polling - " + jsonResponse);
        CampaignList response = objectMapper.readValue(jsonResponse.toString(), CampaignList.class);

        if (response.is_modified) {
            final List<Campaign> campaigns = response.campaigns;
            MediaDownloader.downloadAllMedia(campaigns);
            List<Campaign> validatedCampaigns = Campaign.getValidatedCampaignList(campaigns);
            PlayerUtils.processActiveCampaigns(validatedCampaigns);
//            List<Campaign> reValidatedCampaigns = PlayerUtils.excludeAlreadyActiveCampaigns(campaigns);
            replaceCampaigns(validatedCampaigns, new Date());
        } else {
            LOGGER.info("No new data found on polling");
        }

    }

    static class CampaignList {
        public List<Campaign> campaigns;
        public boolean is_modified;
    }



}
