package com.blynq.app.model;

import android.app.Fragment;
import android.app.FragmentManager;
import android.os.AsyncTask;
import android.support.annotation.NonNull;

import com.blynq.app.activities.R;
import com.blynq.app.asyncs.PanePlayerAsyncTask;
import com.blynq.app.fragments.PlayerPane;
import com.blynq.app.properties.PlayerProperties;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Jaydev on 24/04/16.
 */
public class PlayerUtils {

    public enum MediaType {IMAGE, GIF, VIDEO, PDF, WEB, YOUTUBE, PPT, TEXT, AUDIO}
    public enum OrientationType {LANDSCAPE, PORTRAIT}

    // Dynamic variables susceptible to change at runtime
    public static volatile List<PanePlayerAsyncTask> activePanes;
    public static FragmentManager fragmentManager;
    public static volatile boolean isDefaultLayoutSet = false;
    public static ThreadPoolExecutor downloadThreadPool = new ThreadPoolExecutor(3,
            100, // Max pool size, would'nt matter if LinkedBlockingQueue is used
            60,  // Idle timeout
            TimeUnit.SECONDS,
            new LinkedBlockingQueue<Runnable>());

    /**
     * Return conflicting panes from currently active list
     * @param currCampaign
     * @return
     */
    public static List<PanePlayerAsyncTask> getConflictingFragmentsList(Campaign currCampaign) {
        List<PanePlayerAsyncTask> conflictingPanes = new ArrayList<>();
        for (PanePlayerAsyncTask panePlayerAsyncTask : activePanes) {
            final Pane activePane = panePlayerAsyncTask.getCampaign().getPane();
            boolean isConflict = currCampaign.getPane().isConflict(activePane);
            boolean isDirty = panePlayerAsyncTask.isDirty();
            if (isConflict || isDirty) {
                conflictingPanes.add(panePlayerAsyncTask);
            }
        }
        return conflictingPanes;
    }
    @NonNull
    public static String getFileExtension(String fileName) {
        return fileName.substring(fileName.lastIndexOf(".") + 1, fileName.length()).toLowerCase();
    }

    /**
     * Fetches target storage directory path based on media type
     *
     * @param mediaType
     * @return
     */
    public static String getTargetRelativeFolderPath(MediaType mediaType) {
        String relativeFolderPath;
        if (mediaType.equals(MediaType.IMAGE))
            relativeFolderPath = PlayerProperties.imagesFolder;
        else if (mediaType.equals(MediaType.VIDEO))
            relativeFolderPath = PlayerProperties.videosFolder;
        else if (mediaType.equals(MediaType.PDF))
            relativeFolderPath = PlayerProperties.pdfFolder;
        else if (mediaType.equals(MediaType.GIF))
            relativeFolderPath = PlayerProperties.gifFolder;
        else if (mediaType.equals(MediaType.WEB))
            relativeFolderPath = PlayerProperties.webFolder;
        else
            relativeFolderPath = PlayerProperties.othersFolder;

        return relativeFolderPath;
    }

    public static MediaType getMediaTypeFromDocumentType(String document_type) {
        String[] parts = document_type.split("/");
        MediaType mediaType = null;

        if (parts.length != 3)
            return null;

        String docType = parts[1].toLowerCase();
        String extension = parts[2].toLowerCase();


        if (docType.equalsIgnoreCase("image") || docType.equalsIgnoreCase("images")) {
            mediaType = extension.equalsIgnoreCase("gif") ? MediaType.GIF : MediaType.IMAGE;
        } else if (docType.equalsIgnoreCase("video") || docType.equalsIgnoreCase("videos")) {
            mediaType = MediaType.VIDEO;
        } else if (docType.equalsIgnoreCase("audio") || docType.equalsIgnoreCase("audios")) {
            mediaType = MediaType.AUDIO;
        } else if (docType.equalsIgnoreCase("application") && extension.equalsIgnoreCase("pdf")) {
            mediaType = MediaType.PDF;
        } else if (docType.equalsIgnoreCase("ppt")) {
            mediaType = MediaType.PPT;
        } else if (parts[0].equalsIgnoreCase("url") && docType.equalsIgnoreCase("web")) {
            if (extension.equalsIgnoreCase("youtube"))
                mediaType = MediaType.YOUTUBE;
            else
                mediaType = MediaType.WEB;
        } else {
            mediaType = null;
        }

        return mediaType;
    }

    /**
     * @param url
     * @return Returns youtubeId for valid youtube url, null otherwise
     */
    public static String getYouTubeId(String url) {
        String pattern = "(?<=watch\\?v=|/videos/|embed\\/|youtu.be\\/|\\/v\\/|\\/e\\/|watch\\?v%3D|watch\\?feature=player_embedded&v=|%2Fvideos%2F|embed%\u200C\u200B2F|youtu.be%2F|%2Fv%2F)[^#\\&\\?\\n]*";
        Pattern compiledPattern = Pattern.compile(pattern);
        Matcher matcher = compiledPattern.matcher(url); //url is youtube url for which you want to extract the id.
        if (matcher.find()) {
            return matcher.group().trim();
        }
        return null;
    }

    /**
     * Fetches the filename with which a media item is saved
     *
     * @param playlistItem
     * @return
     */
    public static String getLocalStorageFileName(PlaylistItem playlistItem) {
        PlayerUtils.MediaType media_type = playlistItem.getMedia_type();
        String content_type = playlistItem.getContent_type();
        String[] contentTypeElements = content_type.split("/");
        String extension = getFileExtension(playlistItem.getUrl());
        String localFileName = "";
        if (!(media_type.equals(PlayerUtils.MediaType.WEB) || media_type.equals(PlayerUtils.MediaType.YOUTUBE))) {
            if (contentTypeElements[0].equalsIgnoreCase("url")) {
                String tempFileName = playlistItem.getUrl().replace('/', '_');
                localFileName = tempFileName.substring(tempFileName.indexOf("media"), tempFileName.length());
            } else {
                localFileName = media_type.toString() + "_" + playlistItem.getContent_id() + "." + extension;
            }

        }
        return localFileName;
    }

    public static String getDateString(Date date) {
        DateFormat dateFormat = new SimpleDateFormat(PlayerProperties.dateFormatString);
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        return ((date != null) ? dateFormat.format(date) : PlayerProperties.defaultDateString);
    }

    public static String getFragmentName(Pane pane) {
        StringBuilder sb = new StringBuilder();
        sb.append(pane.getLeft_margin()).append('_').append(pane.getTop_margin()).append('_')
                .append(pane.getHeight()).append('_').append(pane.getWidth());
        return sb.toString();
    }

    /**
     * Add a new fragment (a pane) to screen with desired dimensions
     *
     * @param playerPane
     */
    public static void addPaneToScreen(PlayerPane playerPane, String tag) {
        PlayerUtils.fragmentManager.beginTransaction()
                .setCustomAnimations(R.animator.fadein, R.animator.fadeout)
                .add(R.id.fragment_container, playerPane, tag).commit();
    }

    public static void removeFragments(List<PanePlayerAsyncTask> fragmentsList) {
        for (PanePlayerAsyncTask conflictingFragment : fragmentsList) {
            PlayerUtils.removeFragment(conflictingFragment);
        }
    }

    public static void removeFragment(PanePlayerAsyncTask conflictingPaneAsyncTask) {
        String paneName = PlayerUtils.getFragmentName(conflictingPaneAsyncTask.getCampaign().getPane());

        Fragment fragmentToBeRemoved = PlayerUtils.fragmentManager.findFragmentByTag(paneName);
        if (fragmentToBeRemoved != null && fragmentToBeRemoved.isVisible()) {
            PlayerUtils.fragmentManager.beginTransaction()
                    .setCustomAnimations(R.animator.fadeout, R.animator.fadein)
                    .remove(fragmentToBeRemoved).commit();
        }

        if (conflictingPaneAsyncTask.getStatus() == AsyncTask.Status.RUNNING
                || conflictingPaneAsyncTask.getStatus() == AsyncTask.Status.PENDING) {
            if (conflictingPaneAsyncTask.getPlayerPane() != null) {
                conflictingPaneAsyncTask.getPlayerPane().resetNotifiers();
                conflictingPaneAsyncTask.getPlayerPane().removeSubFragments();
            }
            conflictingPaneAsyncTask.cancel(true); // cancelling async task if active
        }
        activePanes.remove(conflictingPaneAsyncTask);
    }

    public static void setDefaultCampaign() {
        String defaultPaneName = getFragmentName(PlayerProperties.defaultPane);
        PlayerUtils.addPaneToScreen(PlayerPane.newInstance(PlayerProperties.defaultPane, false), defaultPaneName);
        PlayerUtils.activePanes.add(new PanePlayerAsyncTask(null,  PlayerProperties.defaultCampaign));
        isDefaultLayoutSet = true;
    }

    /**
     * Set flag dirty for campaigns that are to be deleted
     * @param campaignList
     */
    public static void processActiveCampaigns(List<Campaign> campaignList) {
        List<Campaign> qualifiedCampaigns = Campaign.getQualifiedCampaignsForCurrentTime(campaignList);

        for (PanePlayerAsyncTask pane: activePanes) {
            boolean dirtyFlag = true;
            for (Campaign campaign : qualifiedCampaigns) {
                if (pane.getCampaign().equals(campaign)) {
                    dirtyFlag = false;
                    break;
                }
            }
            pane.setDirty(dirtyFlag);
        }
    }


    public static Date getMaxDate() {
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Date date = null;
        try {
            date = dateFormat.parse("01/01/2099");
        } catch (ParseException e) {
            new Date(System.currentTimeMillis() + 864000000);
        }
        return date;
    }

    public static boolean isActive(Campaign campaign) {
        if (campaign == null)
            return false;
        for (PanePlayerAsyncTask panePlayerAsyncTask: activePanes) {
            if (campaign.equals(panePlayerAsyncTask.getCampaign()))
                return true;
        }
        return false;
    }

    public static PlayerUtils.OrientationType getOrientationType(String orientation) {
        return PlayerUtils.OrientationType.valueOf(orientation) == null ?
                PlayerUtils.OrientationType.LANDSCAPE: PlayerUtils.OrientationType.valueOf(orientation);
    }

}
