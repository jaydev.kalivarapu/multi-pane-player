package com.blynq.app.asyncs;

import android.os.AsyncTask;

import com.blynq.app.fragments.PlayerPane;
import com.blynq.app.helper.MediaHelper;
import com.blynq.app.model.Campaign;
import com.blynq.app.model.PlayerUtils;
import com.blynq.app.model.Playlist;
import com.blynq.app.model.PlaylistItem;
import com.blynq.app.tools.ALogger;

import org.apache.log4j.Logger;

import java.util.Date;

/**
 * TODO - Use Job Scheduler kind of library to schedule tasks
 * Created by Jaydev on 21/07/16.
 */
public class PanePlayerAsyncTask extends AsyncTask<Void, PlaylistItem, String> {

    private PlayerPane playerPane;
    private Campaign campaign;
    private static Logger LOGGER = ALogger.getLogger(PanePlayerAsyncTask.class);
    private boolean isDirty;

    public PanePlayerAsyncTask(PlayerPane playerPane, Campaign campaign) {
        this.playerPane = playerPane;
        this.campaign = campaign;
        isDirty = false;
    }

    public PlayerPane getPlayerPane() {
        return playerPane;
    }

    public Campaign getCampaign() {
        return campaign;
    }

    public boolean isDirty() {
        return isDirty;
    }

    public void setDirty(boolean dirty) {
        isDirty = dirty;
    }

    @Override
    protected String doInBackground(Void... params) {

        // verify if pane is ready
        while (! playerPane.isReady()) {
            if (isDirty()) {
                return "CAMPAIGN TERMINATED"; // TODO - Is this necessary - verify
            }
            LOGGER.info("Not yet ready");
            customSleep(1000, "Unknown exception"); // Send appropriate message
        }

        LOGGER.info("Pane" + PlayerUtils.getFragmentName(campaign.getPane()) + " is ready");

        outerloop:
        while (campaign.getStart_time().compareTo(new Date()) <= 0 && campaign.getEnd_time().compareTo(new Date()) > 0) {
            for (Playlist playlist : campaign.getPlaylists()) {
                for (PlaylistItem playlistItem : playlist.getPlaylist_items()) {

                    long displayTime;
                    final long remainingScheduleTime = (campaign.getEnd_time().getTime() - new Date().getTime()) / 1000; // in seconds

                    if (remainingScheduleTime <= 0 || isCancelled()) {
                        LOGGER.info("Campaign complete");
                        break outerloop;
                    }

                    LOGGER.debug("Publishing new Playlist item " + playlistItem.getMedia_type() + " " + playlistItem.getLocalMediaPath());
                    publishProgress(playlistItem);

                    displayTime = getDisplayTime(playlistItem, remainingScheduleTime);
                    LOGGER.debug("This item will play for " + displayTime + " seconds");

                    String logMessageIfInterrupted = "Fragment " + PlayerUtils.getFragmentName(campaign.getPane()) + " termination signal received ";
                    // Fragment is removed from calling function as it is thread safe, i.e same name conflict is prevented

                    customSleep(displayTime * 1000, logMessageIfInterrupted);

                }
            }
        }

        LOGGER.info("Removing fragment " + PlayerUtils.getFragmentName(campaign.getPane()));
        PlayerUtils.removeFragment(this);
        return "CAMPAIGN COMPLETE";
    }

    private void customSleep(long timeInMillis, String logMessage) {
        try {
            Thread.sleep(timeInMillis);
        } catch (InterruptedException e) {
            LOGGER.info(logMessage);
        }
    }

    private long getDisplayTime(PlaylistItem playlistItem, long remainingTimeInSeconds) {
        long displayTime;
        if (playlistItem.getMedia_type().equals(PlayerUtils.MediaType.PDF)) {
            int numOfPages = MediaHelper.getNumOfPages(playlistItem.getLocalMediaPath());
            final int pdfPlayTime = playlistItem.getDisplay_time() * numOfPages;
            displayTime = (pdfPlayTime < remainingTimeInSeconds) ? pdfPlayTime : remainingTimeInSeconds;
        } else {
            displayTime = (playlistItem.getDisplay_time() < remainingTimeInSeconds) ? playlistItem.getDisplay_time() : remainingTimeInSeconds;
        }
        return displayTime;
    }

    @Override
    protected void onProgressUpdate(PlaylistItem... values) {

        if (playerPane != null) {
            playerPane.changeActiveMedia(values[0]);
        }
        LOGGER.info("Setting media - " + values[0].getLocalMediaPath() + " in fragment " + PlayerUtils.getFragmentName(campaign.getPane()));
    }


}
