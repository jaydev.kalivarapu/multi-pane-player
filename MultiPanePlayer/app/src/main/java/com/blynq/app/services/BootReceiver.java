package com.blynq.app.services;

/**
 * Created by Jaydev on 09/05/16.
 */

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.blynq.app.activities.RegisterActivity;

public class BootReceiver extends BroadcastReceiver {


    @Override
    public void onReceive(Context context, Intent intent) {
        String intentActionString = intent.getAction();
        if ("android.intent.action.BOOT_COMPLETED".equals(intentActionString)
                || "android.intent.action.PACKAGE_REPLACED".equals(intentActionString)) {
            Intent pushIntent = new Intent(context, RegisterActivity.class);
            pushIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(pushIntent);
        }
    }

}