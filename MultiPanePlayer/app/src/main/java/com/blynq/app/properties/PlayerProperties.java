package com.blynq.app.properties;

import com.blynq.app.model.Campaign;
import com.blynq.app.model.Pane;
import com.blynq.app.model.PlayerUtils;
import com.blynq.app.model.Playlist;
import com.blynq.app.model.PlaylistItem;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * Created by Jaydev on 10/07/16.
 */
public class PlayerProperties {

    // Runtime but constant properties
    public static int screenHeight;
    public static int screenWidth;
    public static String androidId;

    // Static properties

    // Urls
    public static final String baseUrl = "http://www.blynq.in/api/player/";

    public static final String registerUrlAppender = "activationKeyValid";
    public static final String schedulesUrlAppender = "getScreenData";
    public static final String updateUrlAppender = "updateAvailable";

    // Developer Keys
    public static final String youtubeDeveloperKey = "AIzaSyAYwphOCbYm_AFgAj0ErlZKPiY7GEHZpDc";

    // Folder structure
    public static final String mediaFolder = "player/media";
    public static final String imagesFolder = "player/media/images";
    public static final String videosFolder = "player/media/videos";
    public static final String pdfFolder = "player/media/pdf";
    public static final String gifFolder = "player/media/gif";
    public static final String webFolder = "player/media/web";
    public static final String othersFolder = "player/media/others";
    public static final String logsFolder = "player/logs";

    public static final String defaultFolder = "player/default/";
    public static final String serFolder = "player/ser/";
    public static final String tempFolder = "player/temp/";

    // General properties
    public static final String dateFormatString = "ddMMyyyyHHmmss";
    public static final String defaultDateString = "01012016000000";

    // Timeouts
    public static final int pollTimeout = 20000; // in millis
    public static final int schedulePollPeriodicity = 30; // in secs
    //poll_periodicity

    // Default objects
    public static final PlaylistItem defaultPlaylistItem =
            new PlaylistItem(0, "Default Playlist Item", " ", 0, 0, "file/image/jpg");
    public static final Pane defaultPane = new Pane(100, 100, 0, 0, 0, PlayerUtils.OrientationType.LANDSCAPE);
    public static final Campaign defaultCampaign = new Campaign(1, getDefaultPlaylistList(), new Date(), new Date(),
            PlayerUtils.getMaxDate(), defaultPane);

    public static List<Playlist> getDefaultPlaylistList() {
        Playlist playlist = new Playlist(Arrays.asList(defaultPlaylistItem), 1, "default" );
        return Arrays.asList(playlist);
    }

}
