package com.blynq.app.tools;

import org.apache.log4j.Logger;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * Created by Jaydev on 21/05/16.
 */
public class ObjectState {

    public static Logger LOGGER = ALogger.getLogger(ObjectState.class);

    public static boolean serializeObject(Object o, String fullFilePath) {

        if (o == null)
            return false;

        LOGGER.info("Serializing campaignpriorityqueue ..");
        try {
            FileOutputStream fileOut = new FileOutputStream(fullFilePath);
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(o);
            out.close();
            fileOut.close();
            LOGGER.info("Serializing complete, file saved at " + fullFilePath);
            return true;
        } catch (IOException e) {
            LOGGER.error("Failed to serialize campaign priority queue due to ", e);
            return false;
        }
    }

    public static Object deserializeObject(String filePath) {
        LOGGER.info("De-serializing campaignpriorityqueue from path " + filePath);

        try {
            FileInputStream fileIn = new FileInputStream(filePath);
            ObjectInputStream in = new ObjectInputStream(fileIn);
            Object o = in.readObject();
            in.close();
            fileIn.close();
            LOGGER.info("Deserializing complete ..");
            return o;
        } catch (IOException | ClassNotFoundException | NullPointerException e) {
            LOGGER.error("No earlier state available or latest available state is corrupt !! \n" +
                    "Exception message - " + e.getMessage());
        }
        return null;
    }
}
