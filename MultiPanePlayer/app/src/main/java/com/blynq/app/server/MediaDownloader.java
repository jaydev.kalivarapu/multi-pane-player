package com.blynq.app.server;

import android.os.Environment;

import com.blynq.app.model.Campaign;
import com.blynq.app.model.PlayerUtils;
import com.blynq.app.model.Playlist;
import com.blynq.app.model.PlaylistItem;
import com.blynq.app.properties.PlayerProperties;
import com.blynq.app.tools.ALogger;
import com.blynq.app.tools.StorageTools;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.List;


/**
 * Created by Jaydev on 11/05/16.
 */
public class MediaDownloader {

    private static Logger LOGGER = ALogger.getLogger(MediaDownloader.class);

    /**
     * Downloads file given in URL, moves it to targetFolder upon download completion.
     * Until completion, it is placed in tempFolder.
     *
     * @param urlString
     * @param tempFolder
     * @param targetFolder
     * @param fileName
     * @return
     */
    public static boolean downloadFile(String urlString, String tempFolder, String targetFolder, String fileName) {
        File tempFolderFile = null;
        File targetFolderFile;
        tempFolderFile = new File(tempFolder, fileName);
        targetFolderFile = new File(targetFolder, fileName);

        if (targetFolderFile.exists()) {
            LOGGER.info("File already exists at " + targetFolderFile.getAbsolutePath());
            return true;
        } else if (tempFolderFile.exists()) {
            LOGGER.info("File already exists at" + tempFolderFile.getAbsolutePath() + " download already in progress");
            return true;
        }

        try {
            FileUtils.copyURLToFile(new URL(urlString), tempFolderFile);
            StorageTools.moveFile(tempFolder, targetFolder, fileName);
            return true;
        } catch (Exception e) {
            LOGGER.error("Error while downloading file from url - " + urlString, e);
            LOGGER.error("Deleting file present in temp folder");
            if (tempFolderFile != null && tempFolderFile.exists())
                tempFolderFile.delete();
            return false;
        }
    }


    public static boolean isDownloadPending(String mediaPath) {
        String absolutePath = new File(mediaPath).getAbsolutePath();
        String fileName = absolutePath.substring(absolutePath.lastIndexOf(File.pathSeparator) + 1, absolutePath.length());
        String tempFolder = Environment.getExternalStorageDirectory() + "/" + PlayerProperties.tempFolder + "/";
        if (new File(tempFolder, fileName).exists())
            return true;
        return false;
    }

    public static boolean isDownloadComplete(String mediaPath) {
        if (new File(mediaPath).exists())
            return true;
        return false;
    }

    public static void downloadAllMedia(List<Campaign> campaigns) {
        for (Campaign campaign : campaigns) {
            for (Playlist playlist : campaign.getPlaylists())
                for (PlaylistItem playlistItem : playlist.getPlaylist_items()) {
                    if (playlistItem.getLocalMediaPath() != null && playlistItem.getLocalMediaPath().trim().length() > 0) {
                        continue;
                    } else {
                        final PlayerUtils.MediaType media_type = playlistItem.getMedia_type();
                        if (localMediaItemRequired(media_type)) {
                            downloadMediaForPlaylistItem(playlistItem);
                        }
                    }
                }
        }
    }

    public static boolean localMediaItemRequired(PlayerUtils.MediaType media_type) {
        return !(media_type.equals(PlayerUtils.MediaType.WEB) || media_type.equals(PlayerUtils.MediaType.YOUTUBE));
    }

    public static void downloadMediaForPlaylistItem(PlaylistItem playlistItem) {
        PlayerUtils.MediaType mediaType = playlistItem.getMedia_type();
        if ((mediaType.equals(PlayerUtils.MediaType.WEB) || mediaType.equals(PlayerUtils.MediaType.YOUTUBE)))
            return;
        final String url = playlistItem.getUrl();
        final String fileName = PlayerUtils.getLocalStorageFileName(playlistItem);
        String relativeFolderPath = PlayerUtils.getTargetRelativeFolderPath(mediaType);
        final String targetFolder = Environment.getExternalStorageDirectory() + "/" + relativeFolderPath;
        String localMediaPath = Environment.getExternalStorageDirectory() + "/" + relativeFolderPath + "/" + fileName;
        playlistItem.setLocalMediaPath(localMediaPath);
        final String tempFolder = Environment.getExternalStorageDirectory() + "/" + PlayerProperties.tempFolder;
        DownloaderRunnable downloaderRunnable = new DownloaderRunnable(url, tempFolder, targetFolder, fileName);
        PlayerUtils.downloadThreadPool.execute(downloaderRunnable);
    }


    public static void main(String[] args) throws IOException {

        /*DownloaderRunnable downloaderRunnable = new DownloaderRunnable("http://test.blynq.in/media/usercontent/user5/converted_Heritage%20sites.mp4",
                "/Users/Jaydev/Desktop/BlYnQ/test/",
                "/Users/Jaydev/Desktop/BlYnQ/test/test2/",
                "conntest.mp4");

        ThreadPoolExecutor downloadThreadPool = new ThreadPoolExecutor(3,
                100, // Max pool size, would'nt matter if LinkedBlockingQueue is used
                60,  // Idle timeout
                TimeUnit.SECONDS,
                new LinkedBlockingQueue<Runnable>());

        downloadThreadPool.execute(downloaderRunnable);*/


        FileUtils.copyURLToFile(new URL("http://www.blynq.in/media/usercontent/user5/converted_Heritage%20sites.mp4"),
                new File("/Users/Jaydev/Desktop/BlYnQ/test/test2/testvid.mp4"));

    }
}
