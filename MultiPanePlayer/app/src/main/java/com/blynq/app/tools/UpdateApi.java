package com.blynq.app.tools;

import android.os.Environment;

import com.blynq.app.activities.BuildConfig;
import com.blynq.app.properties.PlayerProperties;
import com.blynq.app.server.MediaDownloader;
import com.blynq.app.server.ServerComm;

import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;


/**
 * Created by Jaydev on 19/06/16.
 */
public class UpdateApi {

    private static Logger LOGGER = ALogger.getLogger(UpdateApi.class);

    public static void checkUpdate() {
        final String urlString = PlayerProperties.baseUrl + PlayerProperties.updateUrlAppender;
        Map<String, String> jsonEntries = new HashMap<>();
        jsonEntries.put("device_key", PlayerProperties.androidId);
        jsonEntries.put("version_name", BuildConfig.VERSION_NAME);
        final String jsonRequestString = new JSONObject(jsonEntries).toString();

        try {
            JSONObject response = ServerComm.postToServer(urlString, jsonRequestString);
            if (response.getBoolean("is_update_available")) {
                downloadAndInstallUpdatedApk(response.getString("url"));
            }
        } catch (IOException | JSONException e) {
            LOGGER.error("Exception while attempting player update check");
        } catch (Exception ex) {
            LOGGER.error("Exception while updating apk, ignoring it on client. Developer to verify");
        }

    }

    public static String downloadAndInstallUpdatedApk(final String downloadURL) {
        final String tempFolder = Environment.getExternalStorageDirectory() + "/" + PlayerProperties.tempFolder;
        final String targetFolder = Environment.getExternalStorageDirectory() + "/" + PlayerProperties.othersFolder;
        final String fileName = FilenameUtils.getBaseName(downloadURL) + "." + FilenameUtils.getExtension(downloadURL);
        String serFolder = Environment.getExternalStorageDirectory() + "/" + PlayerProperties.serFolder;
        MediaDownloader.downloadFile(downloadURL, tempFolder, targetFolder, fileName); // blocking download
        StorageTools.deleteFilesInFolder(serFolder);
        installAPK(targetFolder + "/" + fileName);

        return targetFolder;
    }

    public static void installAPK(String filename) {
        File file = new File(filename);
        if (file.exists()) {
            try {
                String command;
                command = "pm install -r " + filename;
                Process proc = Runtime.getRuntime().exec(new String[]{"su", "-c", command});
                proc.waitFor();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
