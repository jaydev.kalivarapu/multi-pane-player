package com.blynq.app.model;

import java.io.Serializable;
import java.util.Date;
import java.util.concurrent.PriorityBlockingQueue;

/**
 * Created by Jaydev on 26/04/16.
 */
public class CampaignPriorityQueue implements Serializable {

    private PriorityBlockingQueue<Campaign> campaignQueue;
    private Date lastModified;

    public CampaignPriorityQueue(int defaultSize) {
        campaignQueue = new PriorityBlockingQueue<Campaign>(defaultSize);

    }

    public CampaignPriorityQueue() {
        campaignQueue = new PriorityBlockingQueue<Campaign>(11);
    }

    // Comparator Anonymous class implementation -- not using as Comparator is not serializable
    /*public static Comparator<Campaign> startTimeComparator = new Comparator<Campaign>() {
        @Override
        public int compare(Campaign lhsCampaign, Campaign rhsCampaign) {
            return (int) (lhsCampaign.getStart_time().compareTo(rhsCampaign.getStart_time()));
        }
    };*/

    public PriorityBlockingQueue<Campaign> getCampaignQueue() {
        return campaignQueue;
    }

    public void setCampaignQueue(PriorityBlockingQueue<Campaign> campaignQueue) {
        this.campaignQueue = campaignQueue;
    }

    public Date getLastModified() {
        return lastModified;
    }

    public void setLastModified(Date lastModified) {
        this.lastModified = lastModified;
    }


}
