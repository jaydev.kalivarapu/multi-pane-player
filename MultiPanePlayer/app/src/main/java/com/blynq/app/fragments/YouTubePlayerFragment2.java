package com.blynq.app.fragments;

import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerFragment;

/**
 * Created by Jaydev on 30/07/16.
 */
public class YouTubePlayerFragment2 extends YouTubePlayerFragment {
    private YouTubePlayer mPlayer;

    public static YouTubePlayerFragment2 newInstance(){
        return new YouTubePlayerFragment2();
    }

    @Override
    public void initialize(String s, final YouTubePlayer.OnInitializedListener onInitializedListener) {
        YouTubePlayer.OnInitializedListener listener = new YouTubePlayer.OnInitializedListener() {
            @Override
            public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b) {
                onInitializedListener.onInitializationSuccess(provider, youTubePlayer, b);
                mPlayer = youTubePlayer;
                mPlayer.setShowFullscreenButton(false);
            }

            @Override
            public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {
                onInitializedListener.onInitializationFailure(provider, youTubeInitializationResult);
            }
        };
        super.initialize(s, listener);
    }

    @Override
    public void onDestroyView() {
        if(mPlayer!=null) {

            mPlayer.release();
        }
        super.onDestroyView();
    }

}

