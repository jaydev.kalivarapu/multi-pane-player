package com.blynq.app.asyncs;

import android.os.AsyncTask;

import com.blynq.app.fragments.PlayerPane;
import com.blynq.app.model.PlaylistItem;
import com.blynq.app.server.MediaDownloader;
import com.blynq.app.tools.ALogger;

import org.apache.log4j.Logger;


/**
 * Created by Jaydev on 26/07/16.
 */
public class MediaDownloadNotifier extends AsyncTask<PlaylistItem, PlaylistItem, Void> {

    private PlayerPane playerPane;
    private static Logger LOGGER = ALogger.getLogger(MediaDownloadNotifier.class);

    public MediaDownloadNotifier(PlayerPane playerPane) {
        this.playerPane = playerPane;
    }

    @Override
    protected Void doInBackground(PlaylistItem... playlistItems) {
        String localFilePath = playlistItems[0].getLocalMediaPath();

        if (localFilePath == null || localFilePath.trim().length() == 0) {
            LOGGER.info("Invalid Playlist item, continuing with default image ..");
            return null;
        }

        for (int i = 10; i < playlistItems[0].getDisplay_time(); i += 10) {
            try {
                // Sleep for 10 seconds and verify media file presence again
                Thread.sleep(10000);
            } catch (InterruptedException e) {
                LOGGER.error("Download notifier thread interrupted, exiting from download notifier", e);
            }

            if (isCancelled())
                break;
            LOGGER.info("Checking for file's presence again ..");
            if (MediaDownloader.isDownloadComplete(localFilePath)) {
                LOGGER.info("File available now, setting image, exiting from Download notifier");
                publishProgress(playlistItems[0]);
                return null;
            } else if (MediaDownloader.isDownloadPending(localFilePath)) {
                LOGGER.info("Downloading at - " + localFilePath + " .." );
            } else {
                LOGGER.info("Download has not started at " + localFilePath + " ..");
            }
        }
        return null;
    }

    @Override
    protected void onProgressUpdate(PlaylistItem... playlistItems) {
        playerPane.changeActiveMedia(playlistItems[0]);
    }
}
