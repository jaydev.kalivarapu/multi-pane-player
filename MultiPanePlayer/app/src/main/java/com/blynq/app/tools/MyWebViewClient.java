package com.blynq.app.tools;

import android.webkit.WebView;
import android.webkit.WebViewClient;

/**
 * Created by Jaydev on 24/07/16.
 */
public class MyWebViewClient extends WebViewClient {

    @Override
    public boolean shouldOverrideUrlLoading(WebView view, String url) {
        view.loadUrl(url);
        return true;
    }
}
