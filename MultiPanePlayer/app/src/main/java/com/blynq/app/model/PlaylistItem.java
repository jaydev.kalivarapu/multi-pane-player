package com.blynq.app.model;

import com.itextpdf.text.pdf.PdfReader;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by Jaydev on 07/05/16.
 */
public class PlaylistItem implements Serializable {
//    private static Logger LOGGER = ALogger.getLogger(PlaylistItem.class);
    private int display_time;
    private String title;
    private String url;
    private int playlist_item_id;
    private int content_id;
    private String localMediaPath;
    private String content_type;
    private PlayerUtils.MediaType media_type;

    public PlaylistItem(int display_time, String title, String url, int playlist_item_id,
                        int content_id, String content_type) {
        this.display_time = display_time;
        this.title = title;
        this.url = url;
        this.playlist_item_id = playlist_item_id;
        this.content_id = content_id;
        this.content_type = content_type;
        setMedia_type(content_type);
    }

    public PlaylistItem() {
    }

    @Override
    public boolean equals(Object other) {
        if (other == null) return false;
        if (other == this) return true;
        if (!(other instanceof PlaylistItem)) return false;

        PlaylistItem otherPlaylistItem = (PlaylistItem) other;
        return new EqualsBuilder()
                .append(display_time, otherPlaylistItem.display_time)
                .append(url, otherPlaylistItem.url)
                .append(playlist_item_id, otherPlaylistItem.playlist_item_id)
                .append(content_id, otherPlaylistItem.content_id)
                .append(content_type, otherPlaylistItem.content_type)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 31)
                .append(display_time)
                .append(url)
                .append(playlist_item_id)
                .append(content_id)
                .append(content_type)
                .toHashCode();
    }

    public int getDisplay_time() {
        return display_time;
    }


    public void setDisplay_time(int display_time) {
        this.display_time = display_time;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public String getLocalMediaPath() {
        return localMediaPath;
    }

    public void setLocalMediaPath(String localMediaPath) {
        this.localMediaPath = localMediaPath;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getPlaylist_item_id() {
        return playlist_item_id;
    }

    public void setPlaylist_item_id(int playlist_item_id) {
        this.playlist_item_id = playlist_item_id;
    }

    public int getContent_id() {
        return content_id;
    }

    public void setContent_id(int content_id) {
        this.content_id = content_id;
    }

    public PlayerUtils.MediaType getMedia_type() {
        return media_type;
    }

    public void setMedia_type(String media_type) {
        this.media_type = PlayerUtils.getMediaTypeFromDocumentType(media_type);
    }

    public boolean isValidPlaylistItem() {
        return (url != null && media_type != null && display_time > 0);
    }

    public static List<PlaylistItem> getValidPlaylistItems(List<PlaylistItem> playlistItemList) {
        List<PlaylistItem> validPlaylistItems = new ArrayList<>();
        for (PlaylistItem playlistItem : playlistItemList) {
            if (playlistItem.isValidPlaylistItem())
                validPlaylistItems.add(playlistItem);
        }
        if (validPlaylistItems.size() > 0)
            return validPlaylistItems;
        else
            return null;
    }

    public String getContent_type() {
        return content_type;
    }

    public void setContent_type(String content_type) {
        this.content_type = content_type;
        setMedia_type(content_type);
    }

    /**
     * If invalid path or document, returns -1
     *
     * @return number of pages of a PDF document
     */
    private int getNumOfPages() {
        int numOfPages = -1; // defaulting to -1

        try {
            PdfReader reader = new PdfReader(getLocalMediaPath());
            numOfPages = reader.getNumberOfPages();
            return numOfPages;
        } catch (Exception ex) {
//            LOGGER.error("Exception while parsing document at path " + localMediaPath, ex);
        }
        return numOfPages;
    }
}
