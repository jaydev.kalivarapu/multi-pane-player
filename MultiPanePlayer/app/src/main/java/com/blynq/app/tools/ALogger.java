package com.blynq.app.tools;

import android.os.Environment;

import com.blynq.app.properties.PlayerProperties;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import de.mindpipe.android.logging.log4j.LogConfigurator;

/**
 * Created by Jaydev on 29/05/16.
 */


public class ALogger {

    private final static LogConfigurator logConfigurator = new LogConfigurator();

    public static Logger getLogger(Class clazz) {
        String logsFolderName;
        logsFolderName = Environment.getExternalStorageDirectory().toString() + File.separator + PlayerProperties.logsFolder + "/";
        String logFileName = "player." + getCurrentDateString() + ".log";
        logConfigurator.setFileName(logsFolderName + logFileName);
        logConfigurator.setRootLevel(Level.INFO);
        logConfigurator.setLevel("org.apache", Level.INFO);
        logConfigurator.setUseFileAppender(true);
        logConfigurator.setFilePattern("%d %-5p [%c{2}]-[%L] %m%n");
        logConfigurator.setMaxFileSize(1024 * 1024 * 3); // each log file max size - 3 MB
        logConfigurator.setImmediateFlush(true);
        logConfigurator.configure();
        Logger log = Logger.getLogger(clazz);
        return log;
    }

    public static String getCurrentDateString() {
        DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
        dateFormat.setTimeZone(TimeZone.getTimeZone("IST"));
        return dateFormat.format(new Date());
    }

}
