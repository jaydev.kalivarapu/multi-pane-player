package com.blynq.app.helper;

import com.blynq.app.tools.ALogger;
import com.itextpdf.text.pdf.PdfReader;

import org.apache.log4j.Logger;

import java.io.IOException;

/**
 * Created by Jaydev on 25/07/16.
 */
public class MediaHelper {
    private static Logger LOGGER = ALogger.getLogger(MediaHelper.class);


    public static int getNumOfPages(String mediaPath) {
        int numOfPages = 1; // defaulting to 1
        try {
            PdfReader reader = new PdfReader(mediaPath);
            numOfPages = reader.getNumberOfPages();
        } catch (IOException e) {
            LOGGER.error("Unable to read document " + mediaPath);
        }
        return numOfPages;
    }
}
